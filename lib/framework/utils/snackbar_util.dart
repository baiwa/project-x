import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

enum TypeSnackbar { complete, error }

class ShowSnackbar {
  // ---- Use ----
  // GlobalKey<ScaffoldState> _key = GlobalKey();
  //
  // Scaffold(
  //       key: _key,
  //

  static show({
    @required GlobalKey<ScaffoldState> scaffoldKey,
    @required String text,
    TypeSnackbar type = TypeSnackbar.complete,
  }) {
    scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text(
          text,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
            fontFamily: GoogleFonts.kanit().fontFamily,
          ),
        ),
        backgroundColor: type == TypeSnackbar.complete ? Colors.green : Colors.red,
        duration: Duration(seconds: 2),
      ),
    );
  }
}
