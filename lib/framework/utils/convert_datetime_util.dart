import 'package:intl/intl.dart';

class ConvertDateTimeUtils {
  static String ddmmyyyy = 'dd/MM/yyyy';
  static String yyyymmdd = 'yyyy/HH/dd';
  static String ddmmyyyyhhmm = 'dd/MM/yyyy HH:mm';
  static String yyyymmddhhmm = 'yyyy/HH/dd HH:mm';
  static String yyyymmddThhmm00Z = "yyyy-HH-dd'T'HH:mm:ss.'000Z'";
  static String hhmm = 'HH:mm';

  static DateTime parseStringToDate(String dateTime, String formatIn) {
    return DateFormat(formatIn).parse(dateTime);
  }

  static String formatDatetimeToString(DateTime datetime, String formatOut) {
    return DateFormat(formatOut).format(datetime);
  }

  static String parseStringFormat(String datetimeStrIn, String formatIn, String formatOut) {
    DateTime date = parseStringToDate(datetimeStrIn, formatIn);
    return formatDatetimeToString(date, formatOut);
  }
}
