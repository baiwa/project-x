import 'package:shared_preferences/shared_preferences.dart';

class SharedDataLocalService {
  SharedDataLocalService._();

  static void writePreferences<T>(String key, T value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    // prefs.setString(key, value);
    if (T == bool) {
      prefs.setBool(key, value as bool);
    } else if (T == int) {
      prefs.setInt(key, value as int);
    } else if (T == double) {
      prefs.setDouble(key, value as double);
    } else if (T == List) {
      prefs.setStringList(key, value as List<String>);
    } else {
      prefs.setString(key, value as String);
    }
  }

  static void deletePreferences(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }

  static Future<T> getValue<T>(String key, {type}) async {
    T value;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      if (T == bool) {
        value = prefs.getBool(key) as T;
      } else if (T == int) {
        value = prefs.getBool(key) as T;
      } else if (T == double) {
        value = prefs.getBool(key) as T;
      } else if (T == List) {
        value = prefs.getStringList(key) as T;
      } else {
        value = prefs.getString(key) as T;
      }
    } catch (e) {
      print('error get sharedPreferences' + e);
    }

    return value;
  }
}
