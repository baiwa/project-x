import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:projectx/framework/service/shared_preferences.dart';

import 'package:dio/dio.dart';
import 'package:projectx/project/commons/global_var.dart';
import 'package:projectx/project/provider/authen_pvd.dart';
import 'package:provider/provider.dart';

class HttpClientService {
  HttpClientService._();
  static String contextPath = 'http://192.168.1.170:3000/';
  // static String contextPath = 'http://192.168.1.142/';

  static Future<dynamic> get(String url, {bool hasAuthorization = true}) async {
    print("Get:" + contextPath + url);
    Map<String, String> headers;
    if (hasAuthorization) headers = await _getHeader(hasAuthorization);
    Response response;
    dynamic dataRes;
    try {
      response = await Dio().get(
        contextPath + url,
        options: Options(
          headers: headers,
          validateStatus: (status) {
            return true;
          },
        ),
      );
      dataRes = _checkDataFromStatus(response, hasAuthorization);
    } catch (e) {
      dataRes = null;
    }
    return dataRes;
  }

  static Future<dynamic> post(String url, dynamic body,
      {bool hasAuthorization = true, bool isFormData = false}) async {
    Map<String, String> headers;
    if (hasAuthorization)
      headers = await _getHeader(hasAuthorization, isFormData: isFormData);
    body = isFormData ? body : json.encode(body);
    Response response;
    dynamic dataRes;
    try {
      response = await Dio().post(
        contextPath + url,
        data: body,
        options: Options(
          headers: headers,
          validateStatus: (status) {
            return true;
          },
        ),
      );
      print("response $response");
      dataRes = _checkDataFromStatus(response, hasAuthorization);
    } catch (e) {
      dataRes = null;
    }
    return dataRes;
  }

  static Future<dynamic> put(String url, dynamic body,
      {bool hasAuthorization = true, bool isFormData = false}) async {
    Map<String, String> headers;
    if (hasAuthorization)
      headers = await _getHeader(hasAuthorization, isFormData: isFormData);
    body = isFormData ? body : json.encode(body);
    Response response;
    dynamic dataRes;
    try {
      response = await Dio().put(
        contextPath + url,
        data: body,
        options: Options(
          headers: headers,
          validateStatus: (status) {
            return true;
          },
        ),
      );
      dataRes = _checkDataFromStatus(response, hasAuthorization);
    } catch (e) {
      dataRes = null;
    }
    return dataRes;
  }

  static Future<dynamic> delete(String url,
      {bool hasAuthorization = true}) async {
    Map<String, String> headers;
    if (hasAuthorization) headers = await _getHeader(hasAuthorization);
    Response response;
    dynamic dataRes;
    try {
      response = await Dio().delete(
        contextPath + url,
        options: Options(
          headers: headers,
          validateStatus: (status) {
            return true;
          },
        ),
      );
      dataRes = _checkDataFromStatus(response, hasAuthorization);
    } catch (e) {
      dataRes = null;
    }
    return dataRes;
  }

  static dynamic _checkDataFromStatus(
      Response response, bool hasAuthorization) {
    var dataRes;
    int status = response.statusCode;
    if (status == 200 || status == 201) {
      dataRes = response?.data != null ? response.data : null;
    } else if (status == 401) {
      if (hasAuthorization) {
        AuthenPvd authProvider =
            Provider.of<AuthenPvd>(contextGlobal, listen: false);
        authProvider.setIsLogin = false;
        Fluttertoast.showToast(
          msg: 'sesion หมดอายุ',
          backgroundColor: Colors.grey,
          textColor: Colors.white,
        );
        return true;
      }
      dataRes = null;
    } else if (status == 400) {
      dataRes = null;
    } else {
      dataRes = null;
    }
    return dataRes;
  }

  static Future<Map<String, String>> _getHeader(bool hasAuthorization,
      {bool isFormData = false}) async {
    String token =
        await SharedDataLocalService.getValue('baiwa-projext-x-token');
    Map<String, String> headers;
    headers = {
      if (!isFormData) "Content-Type": "application/json",
      if (hasAuthorization) "Authorization": "Bearer " + token,
    };
    return headers;
  }
}
