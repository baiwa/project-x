import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:projectx/project/commons/global_var.dart';
import 'package:projectx/project/page/login_page/ui/main_login_page.dart';
import 'package:projectx/project/page/main_wg.dart';
import 'package:projectx/project/provider/authen_pvd.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:projectx/project/commons/constants/theme.dart';
import 'package:projectx/project/commons/utils/theme.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await FlutterDownloader.initialize();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var delegate = await LocalizationDelegate.create(fallbackLocale: 'en', supportedLocales: ['en', 'th']);
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider<ThemeNotifier>(create: (_) => ThemeNotifier(darkTheme)),
        ChangeNotifierProvider<AuthenPvd>(create: (_) => AuthenPvd()..init()),
      ],
      child: LocalizedApp(delegate, BaiwaRootApp(prefs: prefs)),
    ),
  );
}

class Test extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(),
      ),
    );
  }
}

class BaiwaRootApp extends StatelessWidget {
  final SharedPreferences prefs;
  BaiwaRootApp({@required this.prefs});

  @override
  Widget build(BuildContext context) {
    contextGlobal = context;
    final localizationDelegate = LocalizedApp.of(context).delegate;
    final themeNotifier = Provider.of<ThemeNotifier>(context);

    String languageSetting = prefs.getString('baiwa-project-x-languageSetting');
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return LocalizationProvider(
      state: LocalizationProvider.of(context).state,
      child: MaterialApp(
        // local  setting
        localizationsDelegates: [GlobalMaterialLocalizations.delegate, GlobalWidgetsLocalizations.delegate, localizationDelegate],
        supportedLocales: localizationDelegate.supportedLocales,
        locale: languageSetting == null ? localizationDelegate.currentLocale : Locale(languageSetting),
        //-- local  setting

        title: 'BAIWA PROJECT-X',
        theme: themeNotifier.getTheme(),
        home: CheckAuthen(),
      ),
    );
  }
}

class CheckAuthen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // check permission write file
    Future<bool> _checkPermission() async {
      if (Theme.of(context).platform == TargetPlatform.android) {
        PermissionStatus permission = await PermissionHandler().checkPermissionStatus(PermissionGroup.storage);
        if (permission != PermissionStatus.granted) {
          Map<PermissionGroup, PermissionStatus> permissions = await PermissionHandler().requestPermissions([PermissionGroup.storage]);
          if (permissions[PermissionGroup.storage] == PermissionStatus.granted) {
            return true;
          }
        } else {
          return true;
        }
      } else {
        return true;
      }
      return false;
    }

    _checkPermission();

    DateTime currentBackPressTime;
    Future<bool> onWillPop() {
      DateTime now = DateTime.now();
      if (currentBackPressTime == null || now.difference(currentBackPressTime) > Duration(seconds: 2)) {
        currentBackPressTime = now;
        Fluttertoast.showToast(
          msg: 'Press back again to exit.',
          backgroundColor: Colors.grey,
          textColor: Colors.white,
        );
        return Future.value(false);
      }
      return Future.value(true);
    }

    AuthenPvd authProvider = Provider.of<AuthenPvd>(context, listen: true);
    return WillPopScope(
      onWillPop: onWillPop,
      child: authProvider.isLogin ? MyHomePage() : LoginPage(),
    );
  }
}
