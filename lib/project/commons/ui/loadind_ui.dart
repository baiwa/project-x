import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:projectx/project/commons/constants/theme.dart';

class LoaddingUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SpinKitFoldingCube(
      color: ColorsCustom.green,
    );
  }
}
