import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

final String fontFamilyPimary = GoogleFonts.kanit().fontFamily;
final darkTheme = ThemeData(
  fontFamily: GoogleFonts.prompt().fontFamily,
  primarySwatch: Colors.grey,
  primaryColor: ColorsCustom.black,
  brightness: Brightness.dark,
  backgroundColor: ColorsCustom.black.shade400,
  accentColor: Colors.white,
  accentIconTheme: IconThemeData(color: Colors.black),
  dividerColor: Colors.black12,
  dividerTheme: DividerThemeData(color: Colors.grey.shade500),
  appBarTheme: AppBarTheme(
    brightness: Brightness.dark,
  ),
  textTheme: TextTheme(
    display3: TextStyle(color: Colors.white),
  ),
);

final lightTheme = ThemeData(
  fontFamily: GoogleFonts.prompt().fontFamily,
  primarySwatch: Colors.grey,
  primaryColor: Colors.white,
  brightness: Brightness.light,
  backgroundColor: const Color(0xFFE5E5E5),
  accentColor: Colors.black,
  accentIconTheme: IconThemeData(color: Colors.white),
  dividerColor: Colors.white54,
  dividerTheme: DividerThemeData(color: Colors.grey.shade300),
);

class ColorsCustom {
  ColorsCustom._();
  static const MaterialColor black = MaterialColor(
    _blackPrimaryValue,
    <int, Color>{
      50: Color(0xFF798bae),
      100: Color(0xFF54688d),
      200: Color(0xFF3e5277),
      300: Color(0xFF2c3850),
      400: Color(0xFF263147),
      500: Color(_blackPrimaryValue),
    },
  );
  static const int _blackPrimaryValue = 0xFF1d2637;

  static const MaterialColor green = MaterialColor(
    _greenPrimaryValue,
    <int, Color>{
      50: Color(0xFFc5f2d2),
      100: Color(0xFF9ce7b2),
      200: Color(0xFF72de92),
      300: Color(0xFF49d071),
      400: Color(0xFF38c562),
      500: Color(0xFF2ab754),
      600: Color(_greenPrimaryValue),
    },
  );
  static const int _greenPrimaryValue = 0xFF26b851;
}
