class GetUserDetail {
  String username;
  String firstName;
  String lastName;
  String company;
  String email;
  String phone;
  String jobPosition;
  String imgProfile;
  String userRule;
  GetUserDetail(
      {this.username,
      this.firstName,
      this.lastName,
      this.company,
      this.email,
      this.phone,
      this.jobPosition,
      this.imgProfile,
      this.userRule});

  GetUserDetail.fromJson(Map<String, dynamic> json) {
    username = json['username'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    company = json['company'];
    email = json['email'];
    phone = json['phone'];
    jobPosition = json['job_position'];
    imgProfile = json['img_profile'];
    userRule = json['user_rule'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['company'] = this.company;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['job_position'] = this.jobPosition;
    data['img_profile'] = this.imgProfile;
    data['user_rule'] = this.userRule;
    return data;
  }
}
