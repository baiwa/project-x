class GetLeaveModel {
  String sendBy;
  String sendTo;
  String fileUpload;
  String fileUploadName;
  String title;
  String leaveType;
  String detail;
  String createdBy;
  String createdDate;
  String isDeleted;

  GetLeaveModel(
      {this.sendBy,
      this.sendTo,
      this.fileUpload,
      this.fileUploadName,
      this.title,
      this.leaveType,
      this.detail,
      this.createdBy,
      this.createdDate,
      this.isDeleted});

  GetLeaveModel.fromJson(Map<String, dynamic> json) {
    sendBy = json['sendBy'];
    sendTo = json['sendTo'];
    fileUpload = json['fileUpload'];
    fileUploadName = json['fileUploadName'];
    title = json['title'];
    leaveType = json['leaveType'];
    detail = json['detail'];
    createdBy = json['createdBy'];
    createdDate = json['createdDate'];
    isDeleted = json['isDeleted'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['sendBy'] = this.sendBy;
    data['sendTo'] = this.sendTo;
    data['fileUpload'] = this.fileUpload;
    data['fileUploadName'] = this.fileUploadName;
    data['title'] = this.title;
    data['leaveType'] = this.leaveType;
    data['detail'] = this.detail;
    data['createdBy'] = this.createdBy;
    data['createdDate'] = this.createdDate;
    data['isDeleted'] = this.isDeleted;
    return data;
  }
}

class SendLeaveModel {
  List<String> sendTo;
  String title;
  String detail;
  String leaveType;
  List<String> fileUpload;
  List<String> fileUploadName;
  String startDate;
  String endDate;
  String dateType;
  SendLeaveModel(
      {this.sendTo,
      this.title,
      this.detail,
      this.leaveType,
      this.fileUpload,
      this.startDate,
      this.endDate,
      this.dateType,
      this.fileUploadName});

  SendLeaveModel.fromJson(Map<String, dynamic> json) {
    sendTo = json['sendTo'];
    title = json['title'];
    detail = json['detail'];
    leaveType = json['leaveType'];
    fileUpload = json['fileUpload'].cast<String>();
    fileUploadName = json['fileUploadName'].cast<String>();
    startDate = json['startDate'];
    endDate = json['endDate'];
    dateType = json['dateType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['sendTo'] = this.sendTo;
    data['title'] = this.title;
    data['detail'] = this.detail;
    data['leaveType'] = this.leaveType;
    data['fileUpload'] = this.fileUpload;
    data['fileUploadName'] = this.fileUploadName;
    data['startDate'] = this.startDate;
    data['endDate'] = this.endDate;
    data['dateType'] = this.dateType;
    return data;
  }
}
