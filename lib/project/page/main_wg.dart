import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:projectx/framework/service/http_client_service.dart';
import 'package:projectx/framework/utils/downloader_utils.dart';
import 'package:projectx/project/commons/constants/theme.dart';
import 'package:projectx/project/commons/global_var.dart';
import 'package:projectx/project/commons/ui/loadind_ui.dart';
import 'package:projectx/project/models/get_user_detail.dart';
import 'package:projectx/project/page/bottom_bar/bottombar_menu.dart';
import 'package:projectx/project/page/bottom_bar/bottombar_wg.dart';
import 'package:projectx/project/page/delivery_page/fare.dart';
import 'package:projectx/project/page/email_page/email.dart';
import 'package:projectx/project/page/history/history_upload.dart';
import 'package:projectx/project/page/home_page/home_page.dart';
import 'package:projectx/project/page/leave_page/leave_page.dart';
import 'package:projectx/project/page/setting/setting_list_wg.dart';
import 'package:provider/provider.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final PageStorageBucket bucket = PageStorageBucket();
  int _selectedIndex = 0;

  final List<Widget> pages = [
    // HomePage(key: PageStorageKey('Page1')),
    // DeliveryPage(key: PageStorageKey('Page2')),
    // ListPage(key: PageStorageKey('Page3')),
    // SettingListWg(key: PageStorageKey('Page4')),
    HomePage(),
    Fare(),
    LeavePage(),
    Email(),
    HistoryUpload(),
    SettingListWg(),
  ];

  @override
  void initState() {
    bottombarController = StreamController<int>();
    Stream stream = bottombarController.stream;
    stream.listen((value) {
      print(value);
      setState(() => _selectedIndex = value);
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    bottombarController.close();
  }

  Future<bool> getDataUser() async {
    dataUserDetail =
        GetUserDetail.fromJson(await HttpClientService.get('users/user-by-id'));
    log(dataUserDetail.userRule.toString());
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getDataUser(),
        builder: (context, snapshot) {
          return snapshot.hasData
              ? Scaffold(
                  body: SizedBox.expand(
                    child: PageStorage(
                      child: pages[_selectedIndex],
                      bucket: bucket,
                    ),
                  ),
                  bottomNavigationBar: BottomNavyBar(
                    backgroundColor: Theme.of(context).primaryColor,
                    selectedIndex: _selectedIndex,
                    onItemSelected: (index) {
                      setState(() => _selectedIndex = index);
                    },
                    items: listMenu,
                  ),
                )
              : Scaffold(
                  backgroundColor: ColorsCustom.black,
                  body: SizedBox.expand(
                    child: LoaddingUI(),
                  ),
                );
        });
  }
}

class MyAppMM extends StatelessWidget {
  final _images = [
    {
      'name': 'Arches National Park',
      'link':
          'https://upload.wikimedia.org/wikipedia/commons/6/60/The_Organ_at_Arches_National_Park_Utah_Corrected.jpg'
    },
    {
      'name': 'Canyonlands National Park',
      'link':
          'https://upload.wikimedia.org/wikipedia/commons/7/78/Canyonlands_National_Park%E2%80%A6Needles_area_%286294480744%29.jpg'
    },
    {
      'name': 'Death Valley National Park',
      'link':
          'https://upload.wikimedia.org/wikipedia/commons/b/b2/Sand_Dunes_in_Death_Valley_National_Park.jpg'
    },
    {
      'name': 'Gates of the Arctic National Park and Preserve',
      'link':
          'https://upload.wikimedia.org/wikipedia/commons/e/e4/GatesofArctic.jpg'
    },
    {
      'name': 'my',
      'link':
          'http://192.168.1.136:3000/file-manager/download-img/0b9b304c8b787cf28bf6dbd4bcc8732f.jpeg'
    },
  ];

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<DownloadUtil>(
      create: (_) => DownloadUtil()..init(_images, Theme.of(context).platform),
      child: Consumer<DownloadUtil>(
        builder: (context, pvd, child) {
          return pvd.isLoading
              ? new Center(
                  child: new CircularProgressIndicator(),
                )
              : pvd.permissionReady
                  ? new Container(
                      child: new ListView(
                        padding: const EdgeInsets.symmetric(vertical: 16.0),
                        children: pvd.items
                            .map((item) => item.task == null
                                ? new Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16.0, vertical: 8.0),
                                    child: Text(
                                      item.name,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.blue,
                                          fontSize: 18.0),
                                    ),
                                  )
                                : new Container(
                                    padding: const EdgeInsets.only(
                                        left: 16.0, right: 8.0),
                                    child: InkWell(
                                      onTap: item.task.status ==
                                              DownloadTaskStatus.complete
                                          ? () {
                                              pvd
                                                  .openDownloadedFile(item.task)
                                                  .then((success) {
                                                if (!success) {
                                                  Scaffold.of(context)
                                                      .showSnackBar(SnackBar(
                                                          content: Text(
                                                              'Cannot open this file')));
                                                }
                                              });
                                            }
                                          : null,
                                      child: new Stack(
                                        children: <Widget>[
                                          new Container(
                                            width: double.infinity,
                                            height: 64.0,
                                            child: new Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: <Widget>[
                                                new Expanded(
                                                  child: new Text(
                                                    item.name,
                                                    maxLines: 1,
                                                    softWrap: true,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                  ),
                                                ),
                                                new Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 8.0),
                                                  child: pvd.buildActionForTask(
                                                      item.task),
                                                ),
                                              ],
                                            ),
                                          ),
                                          item.task.status ==
                                                      DownloadTaskStatus
                                                          .running ||
                                                  item.task.status ==
                                                      DownloadTaskStatus.paused
                                              ? new Positioned(
                                                  left: 0.0,
                                                  right: 0.0,
                                                  bottom: 0.0,
                                                  child:
                                                      new LinearProgressIndicator(
                                                    value: item.task.progress /
                                                        100,
                                                  ),
                                                )
                                              : new Container()
                                        ]
                                            .where((child) => child != null)
                                            .toList(),
                                      ),
                                    ),
                                  ))
                            .toList(),
                      ),
                    )
                  : new Container(
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 24.0),
                              child: Text(
                                'Please grant accessing storage permission to continue',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.blueGrey, fontSize: 18.0),
                              ),
                            ),
                            SizedBox(
                              height: 32.0,
                            ),
                            FlatButton(
                              onPressed: () {
                                pvd.checkPermission().then((hasGranted) {
                                  pvd.setPermissionReady = hasGranted;
                                });
                              },
                              child: Text(
                                'Retry',
                                style: TextStyle(
                                    color: Colors.blue,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20.0),
                              ),
                            )
                          ],
                        ),
                      ),
                    );
        },
      ),
    );
  }
}
