import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:projectx/framework/service/http_client_service.dart';
import 'package:projectx/project/commons/constants/theme.dart';
import 'package:projectx/project/commons/constants/user_rule.dart';
import 'package:projectx/project/commons/global_var.dart';
import 'package:projectx/project/page/delivery_page/vo/fare_list_data.dart';
import 'package:intl/intl.dart';
import 'package:projectx/project/page/delivery_page/vo/fare_list_master.dart';

class Fare extends StatefulWidget {
  Fare({Key key}) : super(key: key);
  @override
  _FareState createState() => _FareState();
}

void main() {
  runApp(new MaterialApp(
    home: new Fare(),
  ));
}

class _FareState extends State<Fare> {
  TextEditingController topicController = TextEditingController();
  TextEditingController detailController = TextEditingController();
  TextEditingController moneyController = TextEditingController();
  TextEditingController imageFile = TextEditingController();
  PageController _controller;
  List<Travelxpenses> topicData = [];
  List<FareListAll> fareListAll = [];
  List<FareListAllMaster> fareListAllMaster = [];

  @override
  void initState() {
    _controller = PageController(
      initialPage: 0,
    );

    if (0 == fareListAll.length) {
      getAllData();
    }
    if (userRuleAdmin.toLowerCase() == dataUserDetail.userRule.toLowerCase()) {
      getAllDataAdmin();
    }

    super.initState();
  }

  void sentData(String topic, String money, String detail) async {
    Travelxpenses topics = new Travelxpenses();
    topics
      ..date = new DateTime.now()
      ..topic = topic
      ..detail = detail
      ..money = money
      ..imageFile =
          'https://image.freepik.com/free-photo/people-wearing-protective-masks-are-showing-stop-sign-by-hands_1157-31391.jpg';

    topicController.clear();
    moneyController.clear();
    detailController.clear();

    var res = await HttpClientService.post('fare/save', {
      "header": topic,
      "detail": detail,
      "fareMoney": money,
    });

    @override
    void dispose() {
      imageFile.dispose();
      super.dispose();
    }

    setState(() {});
  }

  Future<bool> getAllData() async {
    var res = await HttpClientService.get('fare/byname');

    fareListAll.clear();
    res.forEach((item) {
      fareListAll.add(FareListAll.fromJson(item));
    });
    return true;
  }

  Future<bool> getAllDataAdmin() async {
    var res = await HttpClientService.get('fare/getall-admin');
    fareListAllMaster.clear();
    res.forEach((item) {
      fareListAllMaster.add(FareListAllMaster.fromJson(item));
    });
    return true;
  }

  Widget pageShowUser(BuildContext context) {
    return FutureBuilder(
      future: getAllData(),
      builder: (context, snapshot) {
        if (snapshot.hasData)
          return SingleChildScrollView(
            child: Column(
              children: fareListAll.map((item) {
                print(fareListAll.length);
                return CardUser(
                    item.createdDate,
                    item.header,
                    item.fareMoney,
                    item.detail,
                    'https://image.freepik.com/free-photo/people-wearing-protective-masks-are-showing-stop-sign-by-hands_1157-31391.jpg');
              }).toList(),
            ),
          );
        else
          return SpinKitFoldingCube(
            color: ColorsCustom.green,
          );
      },
    );
  }

  Widget pageShowAdmin(BuildContext context) {
    return FutureBuilder(
      future: getAllDataAdmin(),
      builder: (context, snapshot) {
        if (snapshot.hasData)
          return SingleChildScrollView(
            child: Column(
              children: fareListAllMaster.map((item) {
                print(fareListAllMaster.length);
                return CardAdmin(
                  item.createdBy,
                  item.fareMoney,
                  item.fareMoney,
                );
              }).toList(),
            ),
          );
        else
          return SpinKitFoldingCube(
            color: ColorsCustom.green,
          );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: new AppBar(
        title: new Text('Travel expenses'),
      ),
      body: new Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 30, bottom: 30),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text("User"),
                  ],
                ),
                SizedBox(
                    child: userRuleAdmin.toLowerCase() ==
                            dataUserDetail.userRule.toLowerCase()
                        ? Row(
                            children: <Widget>[
                              Text("Admin"),
                            ],
                          )
                        : null),
              ],
            ),
          ),
          Expanded(
            child: SafeArea(
              child: userRuleAdmin.toLowerCase() ==
                      dataUserDetail.userRule.toLowerCase()
                  ? PageView(
                      children: <Widget>[
                        pageShowUser(context),
                        pageShowAdmin(context)
                      ],
                    )
                  : pageShowUser(context),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                backgroundColor: Theme.of(context).backgroundColor,
                content: Form(
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: TextFormField(
                            controller: topicController,
                            decoration: const InputDecoration(
                              hintText: 'หัวข้อ',
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: TextFormField(
                            controller: moneyController,
                            decoration:
                                new InputDecoration(labelText: "จำนวนเงิน"),
                            keyboardType: TextInputType.number,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: TextFormField(
                            controller: detailController,
                            maxLines: 5,
                            decoration: const InputDecoration(
                              hintText: 'รายละเอียด',
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: TextFormField(
                            controller: imageFile,
                            decoration: const InputDecoration(
                              hintText: 'แนบไฟล์รูป',
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 5, right: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              RaisedButton(
                                color: Colors.red,
                                child: Text("cancel"),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                              RaisedButton(
                                color: Colors.green,
                                child: Text("save"),
                                onPressed: () {
                                  sentData(
                                    topicController.text,
                                    moneyController.text,
                                    detailController.text,
                                  );
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          );
        },
        child: Icon(Icons.add),
      ),
    );
  }
}

class Travelxpenses {
  DateTime date;
  String topic;
  String detail;
  String money;
  String imageFile;

  Travelxpenses(
      {this.date, this.topic, this.detail, this.money, this.imageFile});
}

class CardUser extends StatelessWidget {
  final String date;
  final String topic;
  final String money;
  final String detail;
  final String imageFile;

  CardUser(this.date, this.topic, this.money, this.detail, this.imageFile);

  @override
  Widget build(BuildContext context) {
    var maxHeight = MediaQuery.of(context).size.height;
    var maxWidth = MediaQuery.of(context).size.width;
    var cardTwoWidth;

    return Container(
      child: SizedBox(
        // width: maxWidth,
        height: maxHeight / 4.70,
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              child: SizedBox(
                // height: 200,
                width: cardTwoWidth = maxWidth / 1.25,
                child: Padding(
                  padding: const EdgeInsets.only(top: 0),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    color: Colors.grey.shade100,
                    child: Padding(
                      padding: EdgeInsets.only(
                          left: 25, right: 0, top: 10, bottom: 10),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              // mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  new DateFormat('EEE, d/M/y ,HH:mm ')
                                      .format(DateTime.parse(date)),
                                  style: TextStyle(
                                      color: ColorsCustom.black.shade100),
                                ),
                                Text(
                                  topic,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  style: TextStyle(
                                      color: ColorsCustom.black.shade200,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  money,
                                  maxLines: 1,
                                  style: TextStyle(
                                      color: ColorsCustom.black.shade100,
                                      fontWeight: FontWeight.bold),
                                  overflow: TextOverflow.ellipsis,
                                ),
                                Text(
                                  detail,
                                  // "We must belive that we are gifted",
                                  style: TextStyle(
                                      color: ColorsCustom.black.shade100),
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 3,
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: cardTwoWidth / 4,
                          ),
                          SizedBox(height: 10),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: SizedBox(
                  width: maxWidth / 4,
                  height: maxWidth / 4,
                  child: Padding(
                      padding: const EdgeInsets.only(bottom: 0, right: 10),
                      child: GestureDetector(
                        onTap: () {
                          showCustomDialogWithImage(context, imageFile);
                        },
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(15.0),
                          child: Image.network(
                            imageFile,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ))),
            ),
          ],
        ),
      ),
    );
  }

  void showCustomDialogWithImage(BuildContext context, String url) {
    Dialog dialogWithImage = Dialog(
      child: ClipRRect(
        // borderRadius: BorderRadius.circular(15.0),
        child: Image.network(
          url,
          fit: BoxFit.cover,
        ),
      ),
    );
    showDialog(
        context: context, builder: (BuildContext context) => dialogWithImage);
  }
}

class TravelxpensesAdmin {
  // DateTime date;
  String username;
  String moneyMonth;
  String moneyYear;

  TravelxpensesAdmin({this.username, this.moneyMonth, this.moneyYear});
}

class CardAdmin extends StatelessWidget {
  final String username;
  final String moneyMonth;
  final String moneyYear;

  CardAdmin(this.username, this.moneyMonth, this.moneyYear);

  @override
  Widget build(BuildContext context) {
    var maxHeight = MediaQuery.of(context).size.height;
    var maxWidth = MediaQuery.of(context).size.width;
    var cardTwoWidth;

    return Container(
      child: SizedBox(
        // width: maxWidth,
        height: maxHeight / 4.70,
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              child: SizedBox(
                // height: 200,
                width: cardTwoWidth = maxWidth / 1.25,
                child: Padding(
                  padding: const EdgeInsets.only(top: 0),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    color: Colors.grey.shade100,
                    child: Padding(
                      padding: EdgeInsets.only(
                          left: 25, right: 0, top: 10, bottom: 10),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              // mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "username",
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  style: TextStyle(
                                      color: ColorsCustom.black.shade200,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  "moneyMonth",
                                  maxLines: 1,
                                  style: TextStyle(
                                      color: ColorsCustom.black.shade100,
                                      fontWeight: FontWeight.bold),
                                  overflow: TextOverflow.ellipsis,
                                ),
                                Text(
                                  "moneyYear",
                                  maxLines: 1,
                                  style: TextStyle(
                                      color: ColorsCustom.black.shade100,
                                      fontWeight: FontWeight.bold),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: cardTwoWidth / 4,
                          ),
                          SizedBox(height: 10),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
