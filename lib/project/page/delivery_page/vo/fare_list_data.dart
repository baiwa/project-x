class FareListAll {
  int fareId;
  String header;
  String fareMoney;
  String detail;
  String createdBy;
  String createdDate;
  String isDeleted;

  FareListAll(
      {this.fareId,
      this.header,
      this.fareMoney,
      this.detail,
      this.createdBy,
      this.createdDate,
      this.isDeleted});

  FareListAll.fromJson(Map<String, dynamic> json) {
    fareId = json['fareId_id'];
    header = json['header'];
    fareMoney = json['fare_money'];
    detail = json['detail'];
    createdBy = json['created_by'];
    createdDate = json['created_date'];
    isDeleted = json['is_deleted'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fareId_id'] = this.fareId;
    data['header'] = this.header;
    data['fare_money'] = this.fareMoney;
    data['detail'] = this.detail;
    data['created_by'] = this.createdBy;
    data['created_date'] = this.createdDate;
    data['is_deleted'] = this.isDeleted;
    return data;
  }
}
