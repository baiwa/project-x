class FareListAllMaster {
  int fareId;
  String fareMoney;
  String createdBy;
  String createdDate;
  String isDeleted;

  FareListAllMaster(
      {this.fareId,
      this.fareMoney,
      this.createdBy,
      this.createdDate,
      this.isDeleted});

  FareListAllMaster.fromJson(Map<String, dynamic> json) {
    fareId = json['fareId_id'];
    fareMoney = json['fare_money'];
    createdBy = json['created_by'];
    createdDate = json['created_date'];
    isDeleted = json['is_deleted'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fareId_id'] = this.fareId;
    data['fare_money'] = this.fareMoney;
    data['created_by'] = this.createdBy;
    data['created_date'] = this.createdDate;
    data['is_deleted'] = this.isDeleted;
    return data;
  }
}
