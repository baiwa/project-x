import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:projectx/project/commons/constants/theme.dart';

class ChatForm extends StatefulWidget {
  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<ChatForm> {
  TextEditingController text = TextEditingController();
  var userName = 'User_One';
  static var fontWeights = FontWeight.normal;
  static var fontStyle = FontStyle.normal;
  static var fontColor = Colors.pink;
  double fontSize = 16;
  TextStyle textStyle2 = TextStyle(color: Colors.white);
  TextStyle textStyle = TextStyle(color: Colors.pink);
  TextStyle textStyleSent = TextStyle(fontWeight: fontWeights, fontStyle: fontStyle, color: fontColor);
  List<Chat> chatData = [];
  void sentData() {
    Chat chatSent = new Chat();
    chatSent.text = text.text;
    chatSent.date = new DateTime.now();
    chatSent.username = userName;
    chatSent.style = textStyleSent;
    text.clear();
    setState(() {
      chatData.add(chatSent);
    });
  }

  void changeFontWeight() {
    if (fontWeights == FontWeight.normal) {
      fontWeights = FontWeight.bold;
    } else {
      fontWeights = FontWeight.normal;
    }
    textStyleSent = TextStyle(fontWeight: fontWeights, fontStyle: fontStyle, color: fontColor, fontSize: fontSize);
  }

  void changeFontStyle() {
    if (fontStyle == FontStyle.normal) {
      fontStyle = FontStyle.italic;
    } else {
      fontStyle = FontStyle.normal;
    }
    textStyleSent = TextStyle(fontWeight: fontWeights, fontStyle: fontStyle, color: fontColor, fontSize: fontSize);
  }

  void changeFontColor(Color color) {
    fontColor = color;
    textStyleSent = TextStyle(fontWeight: fontWeights, fontStyle: fontStyle, color: fontColor, fontSize: fontSize);
  }

  void changeFontSize(double size) {
    fontSize = size;
    textStyleSent = TextStyle(fontWeight: fontWeights, fontStyle: fontStyle, color: fontColor, fontSize: fontSize);
  }

  void swapUser() {
    if (userName != 'User_Two') {
      setState(() {
        userName = 'User_Two';
      });
    } else {
      setState(() {
        userName = 'User_One';
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.swap_horiz),
              color: Colors.white,
              onPressed: () {
                swapUser();
              },
            ),
          ],
          title: Text('Chat'),
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        body: Container(
            child: Align(
                alignment: Alignment.bottomCenter,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(20.0),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: chatData.map((item) {
                              var index = chatData.indexOf(item);
                              if (item.username == userName) {
                                return Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                        child: Card(
                                            color: Colors.pink,
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(30),
                                            ),
                                            child: Column(
                                              children: <Widget>[
                                                Padding(
                                                  padding: EdgeInsets.only(top: 2, bottom: 2, right: 10, left: 10),
                                                  child: Text(
                                                    item.text,
                                                    style: item.style,
                                                  ),
                                                ),
                                              ],
                                            )))
                                  ],
                                );
                              } else {
                                return Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                  Container(
                                      child: Card(
                                          color: Colors.white,
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(30),
                                          ),
                                          child: Column(
                                            children: <Widget>[
                                              Padding(
                                                padding: EdgeInsets.only(top: 2, bottom: 2, right: 10, left: 10),
                                                child: Text(item.text, style: item.style),
                                              ),
                                            ],
                                          )))
                                ]);
                              }
                            }).toList()),
                      ),
                      Card(
                          color: Colors.white,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              PopupMenuButton(
                                onSelected: (result) {
                                  if (result == "changeFontWeight") {
                                    changeFontWeight();
                                  } else if (result == "changeFontStyle") {
                                    changeFontStyle();
                                  }
                                },
                                icon: Icon(
                                  Icons.add,
                                  color: ColorsCustom.black.shade200,
                                ),
                                itemBuilder: (context) => [
                                  PopupMenuItem(
                                      value: "changeFontWeight",
                                      child: Icon(
                                        Icons.format_bold,
                                        color: Colors.pink,
                                      )),
                                  PopupMenuItem(
                                      value: "changeFontStyle",
                                      child: Icon(
                                        Icons.format_italic,
                                        color: Colors.pink,
                                      )),
                                  PopupMenuItem(
                                      value: 1,
                                      // enabled: false,
                                      child: PopupMenuButton(
                                          onSelected: (result) {
                                            switch (result) {
                                              case "white":
                                                changeFontColor(Colors.white);
                                                break;
                                              case "green":
                                                changeFontColor(Colors.green);
                                                break;
                                              case "pink":
                                                changeFontColor(Colors.pink);
                                                break;
                                            }
                                          },
                                          icon: Icon(
                                            Icons.format_color_text,
                                            color: Colors.pink,
                                          ),
                                          itemBuilder: (context) => [
                                                PopupMenuItem(value: "pink", child: Text("Pink")),
                                                PopupMenuItem(value: "white", child: Text("White")),
                                                PopupMenuItem(value: "green", child: Text("Green")),
                                              ])),
                                  PopupMenuItem(
                                      child: PopupMenuButton(
                                          icon: Icon(
                                            Icons.format_size,
                                            color: Colors.pink,
                                          ),
                                          onSelected: (result) {
                                            switch (result) {
                                              case '14':
                                                changeFontSize(14);
                                                break;
                                              case '16':
                                                changeFontSize(16);
                                                break;
                                              case '18':
                                                changeFontSize(18);
                                                break;
                                              case '20':
                                                changeFontSize(20);
                                                break;
                                              case '22':
                                                changeFontSize(22);
                                                break;
                                              case '24':
                                                changeFontSize(24);
                                                break;
                                              case '26':
                                                changeFontSize(26);
                                                break;
                                            }
                                          },
                                          itemBuilder: (context) => [
                                                PopupMenuItem(value: "14", child: Text("14")),
                                                PopupMenuItem(value: "16", child: Text("16")),
                                                PopupMenuItem(value: "18", child: Text("18")),
                                                PopupMenuItem(value: "20", child: Text("20")),
                                                PopupMenuItem(value: "22", child: Text("22")),
                                                PopupMenuItem(value: "24", child: Text("24")),
                                                PopupMenuItem(value: "26", child: Text("26")),
                                              ])),
                                ],
                              ),
                              Expanded(
                                  child: Padding(
                                padding: EdgeInsets.only(bottom: 5),
                                child: TextFormField(
                                  style: TextStyle(color: Colors.pink),
                                  controller: text,
                                  onChanged: (String textcome) {},
                                ),
                              )),
                              IconButton(
                                onPressed: () {
                                  sentData();
                                },
                                icon: Icon(
                                  Icons.send,
                                  color: Colors.pink,
                                ),
                              ),
                            ],
                          )),
                    ],
                  ),
                ))));
  }
}

class Chat {
  TextStyle style;
  DateTime date;
  String username;
  String text;
  Chat({this.username, this.date, this.text, this.style});
}
