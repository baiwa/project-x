import 'package:flutter/material.dart';
import 'package:projectx/project/commons/constants/theme.dart';
import 'package:projectx/project/commons/global_var.dart';
import 'package:projectx/project/commons/service/go_location_service.dart';
import 'package:projectx/project/page/home_page/widget/calendar/calendar_ui.dart';
import 'package:projectx/project/page/home_page/widget/fare/fare_hl_wg.dart';
import 'package:projectx/project/page/home_page/widget/header.dart';
import 'package:projectx/project/page/home_page/widget/highlight/highlight_ui.dart';
import 'package:projectx/project/page/home_page/widget/leave/leave_hl_wg.dart';
import 'package:projectx/project/page/home_page/widget/search/search_page.dart';

class HomePage extends StatelessWidget {
  HomePage({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        title: Row(
          children: <Widget>[
            // Icon(Icons.blur_on, color: ColorsCustom.green),
            // SizedBox(width: 3),
            SizedBox(
              height: 40,
              child: Image.asset("assets/images/logo/baiwa-logo.png"),
            ),
            SizedBox(width: 3),
            Text(
              'BAIWA PROJEXT-X',
              style: TextStyle(
                fontWeight: FontWeight.w100,
                color: ColorsCustom.green,
                // fontSize: 18,
              ),
            ),
          ],
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search, color: ColorsCustom.green),
            // iconSize: 20,
            onPressed: () {
              Navigator.push(context, ScaleRoute(page: SearchPage()));
            },
          )
        ],
      ),
      body: ListView(
        children: <Widget>[
          HomeHeader('Holiday'),
          CalendarUI(),
          HomeHeader(
            'Leave',
            ontap: () {
              bottombarController.add(3);
            },
          ),
          LeaveStatusWg(),
          HomeHeader('Fare'),
          FareHlWg(),
          // ShoppingTypeUI(),
          SizedBox(height: 10),
          HighlightUI(),
          // HighlightSlide(),
          // HighlightSlide(),
          // HigthlightVerticalUI(),
        ],
      ),
    );
  }
}
