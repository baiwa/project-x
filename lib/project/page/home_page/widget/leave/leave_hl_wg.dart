import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:projectx/project/commons/constants/theme.dart';

class LeaveStatusWg extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(10, 0, 10, 5),
        child: Card(
          color: ColorsCustom.black.shade300,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                flex: 10,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.center,
                            child: CircularPercentIndicator(
                              radius: 120.0,
                              lineWidth: 20.0,
                              animation: true,
                              percent: 0.7,
                              animationDuration: 800,
                              reverse: true,
                              center: new Text(
                                "7",
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 30.0,
                                ),
                              ),
                              circularStrokeCap: CircularStrokeCap.round,
                              progressColor: ColorsCustom.green,
                              backgroundColor: ColorsCustom.black.shade200,
                            ),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Container(
                              margin: EdgeInsets.only(left: 85, top: 85),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: ColorsCustom.green.shade300,
                                boxShadow: [
                                  BoxShadow(
                                    color: ColorsCustom.black.shade300,
                                    spreadRadius: 4,
                                  ),
                                ],
                              ),
                              width: 20,
                              height: 20,
                              child: Center(
                                child: Text(
                                  '12',
                                  style: TextStyle(fontSize: 10),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.center,
                            child: CircularPercentIndicator(
                              radius: 120.0,
                              lineWidth: 20.0,
                              animation: true,
                              percent: 0.2,
                              animationDuration: 800,
                              reverse: true,
                              center: new Text(
                                "3",
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 30.0,
                                ),
                              ),
                              circularStrokeCap: CircularStrokeCap.round,
                              progressColor: ColorsCustom.green,
                              backgroundColor: ColorsCustom.black.shade200,
                            ),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Container(
                              margin: EdgeInsets.only(left: 85, top: 85),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: ColorsCustom.green.shade300,
                                boxShadow: [
                                  BoxShadow(
                                    color: ColorsCustom.black.shade300,
                                    spreadRadius: 4,
                                  ),
                                ],
                              ),
                              width: 20,
                              height: 20,
                              child: Center(
                                child: Text(
                                  '15',
                                  style: TextStyle(fontSize: 10),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 3,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Center(child: Text('Personal Leave')),
                    ),
                    Expanded(
                      child: Center(child: Text('Sick Leave')),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
