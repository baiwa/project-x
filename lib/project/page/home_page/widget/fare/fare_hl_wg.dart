import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:projectx/project/commons/constants/theme.dart';

class FareHlWg extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => FareHlWgState();
}

class FareHlWgState extends State<FareHlWg> {
  final Duration animDuration = Duration(milliseconds: 250);

  int touchedIndex;
  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1,
      child: Padding(
        padding: EdgeInsets.fromLTRB(10, 0, 10, 5),
        child: Card(
          color: ColorsCustom.black.shade300,
          child: Stack(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Text(
                      ' ฿780',
                      style: TextStyle(
                        color: Colors.white.withOpacity(0.9),
                        fontSize: 36,
                        fontWeight: FontWeight.w100,
                      ),
                    ),
                    RichText(
                      text: TextSpan(
                        children: <TextSpan>[
                          TextSpan(
                            text: '   +40฿ ',
                            style: TextStyle(
                              fontFamily: fontFamilyPimary,
                              color: ColorsCustom.green.shade100,
                            ),
                          ),
                          TextSpan(
                            text: 'This Mount',
                            style: TextStyle(fontFamily: fontFamilyPimary),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: BarChart(
                          mainBarData(),
                          swapAnimationDuration: animDuration,
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  BarChartGroupData makeGroupData(
    int x,
    double y, {
    bool isTouched = false,
    Color barColor,
    double width,
    List<int> showTooltips = const [],
  }) {
    width ??= MediaQuery.of(context).size.width / 20;
    barColor ??= ColorsCustom.green.shade200;
    return BarChartGroupData(
      x: x,
      barRods: [
        BarChartRodData(
          y: isTouched ? y + 1 : y,
          color: isTouched ? ColorsCustom.green.shade100 : barColor,
          width: width,
          backDrawRodData: BackgroundBarChartRodData(
            show: true,
            y: 18,
            color: ColorsCustom.black.shade500,
          ),
        ),
      ],
      showingTooltipIndicators: showTooltips,
    );
  }

  List<BarChartGroupData> showingGroups() => List.generate(12, (i) {
        switch (i) {
          case 0:
            return makeGroupData(0, 5, isTouched: i == touchedIndex);
          case 1:
            return makeGroupData(1, 6.5, isTouched: i == touchedIndex);
          case 2:
            return makeGroupData(2, 5, isTouched: i == touchedIndex);
          case 3:
            return makeGroupData(3, 7.5, isTouched: i == touchedIndex);
          case 4:
            return makeGroupData(4, 9, isTouched: i == touchedIndex);
          case 5:
            return makeGroupData(5, 11.5, isTouched: i == touchedIndex);
          case 6:
            return makeGroupData(6, 6.5, isTouched: i == touchedIndex);
          case 7:
            return makeGroupData(7, 14.9, isTouched: i == touchedIndex);
          case 8:
            return makeGroupData(8, 12, isTouched: i == touchedIndex);
          case 9:
            return makeGroupData(9, 4.7, isTouched: i == touchedIndex);
          case 10:
            return makeGroupData(10, 0, isTouched: i == touchedIndex);
          case 11:
            return makeGroupData(11, 0, isTouched: i == touchedIndex);
          default:
            return makeGroupData(0, 0, isTouched: i == touchedIndex);
        }
      });

  BarChartData mainBarData() {
    return BarChartData(
      barTouchData: BarTouchData(
        touchTooltipData: BarTouchTooltipData(
            tooltipBgColor: Colors.blueGrey,
            getTooltipItem: (group, groupIndex, rod, rodIndex) {
              String weekDay;
              switch (group.x.toInt()) {
                case 0:
                  weekDay = 'January';
                  break;
                case 1:
                  weekDay = 'February';
                  break;
                case 2:
                  weekDay = 'March';
                  break;
                case 3:
                  weekDay = 'April';
                  break;
                case 4:
                  weekDay = 'May';
                  break;
                case 5:
                  weekDay = 'June';
                  break;
                case 6:
                  weekDay = 'July';
                  break;
                case 7:
                  weekDay = 'August';
                  break;
                case 8:
                  weekDay = 'September';
                  break;
                case 9:
                  weekDay = 'October';
                  break;
                case 10:
                  weekDay = 'November';
                  break;
                case 11:
                  weekDay = 'December';
                  break;
              }
              return BarTooltipItem(
                weekDay + '\n' + (rod.y - 1).toString(),
                TextStyle(color: ColorsCustom.green.shade50),
              );
            }),
        touchCallback: (barTouchResponse) {
          setState(() {
            if (barTouchResponse.spot != null &&
                barTouchResponse.touchInput is! FlPanEnd &&
                barTouchResponse.touchInput is! FlLongPressEnd) {
              touchedIndex = barTouchResponse.spot.touchedBarGroupIndex;
            } else {
              touchedIndex = -1;
            }
          });
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          textStyle: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 14),
          margin: 16,
          getTitles: (double value) {
            bool tabletScreen = MediaQuery.of(context).size.width > 600;
            switch (value.toInt()) {
              case 0:
                return 'Jan';
              case 1:
                return tabletScreen ? 'Feb' : '';
              case 2:
                return 'Mar';
              case 3:
                return tabletScreen ? 'Apr' : '';
              case 4:
                return 'May';
              case 5:
                return tabletScreen ? 'Jun' : '';
              case 6:
                return 'Jul';
              case 7:
                return tabletScreen ? 'Aug' : '';
              case 8:
                return 'Sep';
              case 9:
                return tabletScreen ? 'Oct' : '';
              case 10:
                return 'Nov';
              case 11:
                return tabletScreen ? 'Dec' : '';
              default:
                return '';
            }
          },
        ),
        leftTitles: const SideTitles(
          showTitles: false,
        ),
      ),
      borderData: FlBorderData(
        show: false,
      ),
      barGroups: showingGroups(),
    );
  }
}
