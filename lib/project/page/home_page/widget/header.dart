import 'package:flutter/material.dart';
import 'package:projectx/project/commons/constants/theme.dart';

class HomeHeader extends StatelessWidget {
  final String text;
  final Function ontap;
  HomeHeader(this.text, {this.ontap});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 15, 10, 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            text,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w100,
              color: Color(0xFFc2cce0),
            ),
          ),
          InkWell(
            onTap: ontap,
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Text(
                'See All',
                style: TextStyle(
                  fontSize: 14,
                  color: ColorsCustom.green.shade400,
                  fontWeight: FontWeight.w200,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
