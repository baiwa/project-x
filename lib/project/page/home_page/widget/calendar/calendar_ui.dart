import 'package:flutter/material.dart';
import 'package:projectx/framework/service/http_client_service.dart';
import 'package:projectx/project/commons/constants/theme.dart';
import 'package:projectx/project/commons/ui/loadind_ui.dart';
import 'package:projectx/project/page/home_page/widget/calendar/carlendar_wg.dart';
import 'package:projectx/project/page/home_page/widget/calendar/get_holiday_model.dart';

class CalendarUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    bool haveData = false;
    List<GetHoliday> holidayList = [];

    Future<bool> getData() async {
      var res = await HttpClientService.get('holiday/get/2020');
      (res ?? []).forEach((item) {
        holidayList.add(GetHoliday.fromJson(item));
      });
      return true;
    }

    return FutureBuilder(
      future: getData(),
      builder: (context, snapshot) {
        return Padding(
          padding: EdgeInsets.fromLTRB(10, 0, 10, 5),
          child: Builder(
            builder: (context) {
              return snapshot.hasData
                  ? Column(
                      children: <Widget>[
                        Card(
                          color: ColorsCustom.black.shade300,
                          child: CalendarWg(),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Divider(),
                        ),
                        Card(
                          color: ColorsCustom.black.shade300,
                          child: SizedBox(
                            height: 150,
                            width: double.infinity,
                            child: Column(
                              children: <Widget>[
                                Container(
                                  decoration: BoxDecoration(
                                    color: ColorsCustom.green.shade300,
                                    shape: BoxShape.rectangle,
                                    borderRadius: new BorderRadius.only(
                                      topLeft: Radius.circular(5.0),
                                      topRight: Radius.circular(5.0),
                                    ),
                                  ),
                                  child: SizedBox(
                                    height: 8,
                                    width: double.infinity,
                                  ),
                                ),
                                if (haveData)
                                  Expanded(
                                    child: Container(),
                                  ),
                                if (haveData)
                                  Padding(
                                    padding: EdgeInsets.only(left: 10, right: 10),
                                    child: Divider(),
                                  ),
                                if (haveData)
                                  Expanded(
                                    child: Container(),
                                  ),
                                if (!haveData)
                                  Expanded(
                                    child: Center(
                                      child: Text('No Holiday', style: TextStyle(color: Colors.white54)),
                                    ),
                                  ),
                              ],
                            ),
                          ),
                        )
                      ],
                    )
                  : SizedBox(
                      height: 360,
                      child: Card(
                        color: ColorsCustom.black.shade300,
                        child: Center(child: LoaddingUI()),
                      ),
                    );
            },
          ),
        );
      },
    );
  }
}

class CalendarHlPvd with ChangeNotifier {
  bool loading = true;
  bool haveData = false;

  init() async {
    await Future.delayed(Duration(seconds: 1));
    loading = false;
    notifyListeners();
  }
}
