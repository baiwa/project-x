class GetHoliday {
  String createdDate;
  String createdBy;
  Null updatedBy;
  Null updatedDate;
  String isDeleted;
  int holidayId;
  String holidayDay;
  String holidayDescription;
  String active;

  GetHoliday({this.createdDate, this.createdBy, this.updatedBy, this.updatedDate, this.isDeleted, this.holidayId, this.holidayDay, this.holidayDescription, this.active});

  GetHoliday.fromJson(Map<String, dynamic> json) {
    createdDate = json['createdDate'];
    createdBy = json['createdBy'];
    updatedBy = json['updatedBy'];
    updatedDate = json['updatedDate'];
    isDeleted = json['isDeleted'];
    holidayId = json['holiday_id'];
    holidayDay = json['holidayDay'];
    holidayDescription = json['holidayDescription'];
    active = json['active'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdDate'] = this.createdDate;
    data['createdBy'] = this.createdBy;
    data['updatedBy'] = this.updatedBy;
    data['updatedDate'] = this.updatedDate;
    data['isDeleted'] = this.isDeleted;
    data['holiday_id'] = this.holidayId;
    data['holidayDay'] = this.holidayDay;
    data['holidayDescription'] = this.holidayDescription;
    data['active'] = this.active;
    return data;
  }
}
