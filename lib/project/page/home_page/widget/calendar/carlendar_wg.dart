import 'package:flutter/material.dart';
import 'package:projectx/project/commons/constants/theme.dart';
import 'package:table_calendar/table_calendar.dart';

class CalendarWg extends StatefulWidget {
  @override
  _CalendarWgState createState() => _CalendarWgState();
}

class _CalendarWgState extends State<CalendarWg> with TickerProviderStateMixin {
  AnimationController _animationController;
  CalendarController _calendarController = CalendarController();
  DateTime dateSelect = DateTime.now();

  final Map<DateTime, List> holidays = {
    DateTime(2020, 1, 1): ['New Year\'s Day'],
    DateTime(2020, 1, 6): ['Epiphany'],
    DateTime(2020, 2, 14): ['Valentine\'s Day'],
    DateTime(2020, 4, 21): ['Easter Sunday'],
    DateTime(2020, 4, 22): ['Easter Monday'],
  };

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );

    // _calendarController.setCalendarFormat(CalendarFormat.twoWeeks);

    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    _calendarController.dispose();
    super.dispose();
  }

  void _onDaySelected(DateTime day, List events) {}

  @override
  Widget build(BuildContext context) {
    return _buildTableCalendar();
  }

  Widget _buildTableCalendar() {
    return TableCalendar(
      calendarController: _calendarController,
      // locale: "cn_US",
      // events: _events,
      startingDayOfWeek: StartingDayOfWeek.sunday,
      daysOfWeekStyle: DaysOfWeekStyle(
        weekdayStyle: TextStyle(color: ColorsCustom.black.shade50),
      ),
      holidays: holidays,
      formatAnimation: FormatAnimation.slide,
      calendarStyle: CalendarStyle(
        selectedColor: ColorsCustom.green.shade400,
        todayColor: ColorsCustom.green.shade200,
        markersColor: Colors.brown[500],
        outsideStyle: TextStyle(color: Colors.grey),
        holidayStyle: TextStyle(color: Colors.blue),
        weekendStyle: TextStyle(color: Colors.red.withOpacity(0.8)),
        // outsideDaysVisible: false,
      ),
      availableGestures: AvailableGestures.horizontalSwipe,
      initialCalendarFormat: CalendarFormat.twoWeeks,
      availableCalendarFormats: {
        CalendarFormat.month: 'Month',
        CalendarFormat.twoWeeks: '2Weeks',
      },
      headerStyle: HeaderStyle(
        formatButtonTextStyle: TextStyle().copyWith(
          color: Colors.white.withOpacity(0.8),
          fontSize: 15.0,
        ),
        formatButtonDecoration: BoxDecoration(
          color: ColorsCustom.green.withOpacity(0.7),
          borderRadius: BorderRadius.circular(16.0),
        ),
        leftChevronIcon: Icon(Icons.keyboard_arrow_left),
        rightChevronIcon: Icon(Icons.keyboard_arrow_right),
        // titleTextStyle:
      ),
      onDaySelected: _onDaySelected,
      onVisibleDaysChanged: null,
      builders: CalendarBuilders(
        markersBuilder: (context, date, events, holidays) {
          final children = <Widget>[];

          if (events.isNotEmpty) {
            children.add(
              Positioned(
                right: 1,
                bottom: 1,
                child: _buildEventsMarker(date, events),
              ),
            );
          }

          if (holidays.isNotEmpty) {
            children.add(
              Positioned(
                right: -2,
                top: -2,
                child: _buildHolidaysMarker(),
              ),
            );
          }

          return children;
        },
        todayDayBuilder: (context, date, _) {
          return Container(
            margin: EdgeInsets.all(MediaQuery.of(context).size.width / 40),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: ColorsCustom.green,
            ),
            width: 5,
            height: 5,
            child: Center(
              child: Text('${date.day}'),
            ),
          );
        },
        selectedDayBuilder: (context, date, _) {
          return Container(
            margin: EdgeInsets.all(MediaQuery.of(context).size.width / 40),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: ColorsCustom.green.shade300,
            ),
            width: 5,
            height: 5,
            child: Center(
              child: Text('${date.day}'),
            ),
          );
        },
      ),
    );
  }

  Widget _buildHolidaysMarker() {
    return Icon(
      Icons.add_box,
      size: 20.0,
      color: Colors.blueGrey[800],
    );
  }

  Widget _buildEventsMarker(DateTime date, List events) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: _calendarController.isSelected(date) ? Colors.brown[500] : _calendarController.isToday(date) ? Colors.brown[300] : Colors.blue[400],
      ),
      width: 16.0,
      height: 16.0,
      child: Center(
        child: Text(
          '${events.length}',
          style: TextStyle().copyWith(
            color: Colors.white,
            fontSize: 12.0,
          ),
        ),
      ),
    );
  }
}
