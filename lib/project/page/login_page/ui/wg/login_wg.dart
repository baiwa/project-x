import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:projectx/project/commons/constants/theme.dart';
import 'package:provider/provider.dart';
import '../login_main_provider.dart';

class LoginWg extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final pvd = Provider.of<LoginMainPvd>(context, listen: true);
    return Container(
      padding: EdgeInsets.only(top: 23.0),
      child: Column(
        children: <Widget>[
          Stack(
            alignment: Alignment.topCenter,
            overflow: Overflow.visible,
            children: <Widget>[
              Card(
                elevation: 2.0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Container(
                  width: 300.0,
                  height: 210.0,
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                          top: 20.0,
                          bottom: 20.0,
                          left: 25.0,
                          right: 25.0,
                        ),
                        child: TextField(
                          focusNode: pvd.myFocusNodeEmailLogin,
                          controller: pvd.loginEmailController,
                          keyboardType: TextInputType.emailAddress,
                          textInputAction: TextInputAction.next,
                          onSubmitted: (text) {
                            FocusScope.of(context).requestFocus(pvd.myFocusNodePasswordLogin);
                          },
                          style: TextStyle(fontSize: 16.0, color: Colors.black),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: SvgPicture.asset(
                              'assets/images/bottombar_icon/user.svg',
                            ),
                            hintText: "Username",
                            hintStyle: TextStyle(
                              fontSize: 17.0,
                              color: Colors.black38,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: 250.0,
                        height: 1.0,
                        color: Colors.grey[400],
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20.0, bottom: 20.0, left: 25.0, right: 25.0),
                        child: TextField(
                          focusNode: pvd.myFocusNodePasswordLogin,
                          controller: pvd.loginPasswordController,
                          obscureText: pvd.obscureTextLogin,
                          style: TextStyle(fontSize: 16.0, color: Colors.black),
                          onSubmitted: (text) {
                            if (!pvd.sending) pvd.onLogin();
                          },
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: SvgPicture.asset(
                              'assets/images/bottombar_icon/lock.svg',
                              height: 24,
                            ),
                            hintText: "Password",
                            hintStyle: TextStyle(
                              fontSize: 17.0,
                              color: Colors.black38,
                            ),
                            suffixIcon: GestureDetector(
                              onTap: pvd.toggleLogin,
                              child: Icon(
                                pvd.obscureTextLogin ? FontAwesomeIcons.eye : FontAwesomeIcons.eyeSlash,
                                size: 15.0,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      )
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 190.0),
                decoration: new BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: ColorsCustom.black.shade300,
                      offset: Offset(1.0, 6.0),
                      blurRadius: 20.0,
                    ),
                    BoxShadow(
                      color: ColorsCustom.black.shade500,
                      offset: Offset(1.0, 6.0),
                      blurRadius: 20.0,
                    ),
                  ],
                  gradient: new LinearGradient(
                      colors: [
                        ColorsCustom.green.shade500,
                        ColorsCustom.green.shade200,
                      ],
                      begin: const FractionalOffset(0.2, 0.2),
                      end: const FractionalOffset(1.0, 1.0),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp),
                ),
                child: MaterialButton(
                  highlightColor: Colors.transparent,
                  splashColor: ColorsCustom.green.shade50,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 42.0),
                    child: pvd.sending
                        ? SizedBox(
                            width: 50,
                            child: SpinKitFoldingCube(
                              color: Colors.black54,
                              size: 25,
                            ),
                          )
                        : Text(
                            "LOGIN",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18.0,
                            ),
                          ),
                  ),
                  onPressed: pvd.sending ? () {} : pvd.onLogin,
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.0),
            child: FlatButton(
                onPressed: () {},
                child: Text(
                  "Forgot Password?",
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    color: Colors.white,
                    fontSize: 16.0,
                  ),
                )),
          ),
        ],
      ),
    );
  }
}
