import 'package:flutter/material.dart';
import 'package:projectx/framework/service/http_client_service.dart';
import 'package:projectx/framework/service/shared_preferences.dart';
import 'package:projectx/framework/utils/snackbar_util.dart';
import 'package:projectx/project/provider/authen_pvd.dart';

class LoginMainPvd with ChangeNotifier {
  final BuildContext context;
  final AuthenPvd authProvider;
  LoginMainPvd(this.context, this.authProvider);

  bool _sending = false;
  set setSending(bool send) => {this._sending = send, notifyListeners()};
  bool get sending => this._sending;

  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  final FocusNode myFocusNodeEmailLogin = FocusNode();
  final FocusNode myFocusNodePasswordLogin = FocusNode();

  final FocusNode myFocusNodePassword = FocusNode();
  final FocusNode myFocusNodeEmail = FocusNode();
  final FocusNode myFocusNodeName = FocusNode();

  TextEditingController loginEmailController = new TextEditingController();
  TextEditingController loginPasswordController = new TextEditingController();

  bool obscureTextLogin = true;
  bool obscureTextSignup = true;
  bool obscureTextSignupConfirm = true;

  TextEditingController signupEmailController = new TextEditingController();
  TextEditingController signupNameController = new TextEditingController();
  TextEditingController signupPasswordController = new TextEditingController();
  TextEditingController signupConfirmPasswordController = new TextEditingController();

  PageController pageController;

  void showInSnackBar(String value) {
    FocusScope.of(context).requestFocus(new FocusNode());
    scaffoldKey.currentState?.removeCurrentSnackBar();
    scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(
        value,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.white,
          fontSize: 16.0,
        ),
      ),
      backgroundColor: Colors.blue,
      duration: Duration(seconds: 3),
    ));
  }

  void onSignInButtonPress() {
    pageController.animateToPage(0, duration: Duration(milliseconds: 500), curve: Curves.decelerate);
  }

  void onSignUpButtonPress() {
    pageController?.animateToPage(1, duration: Duration(milliseconds: 500), curve: Curves.decelerate);
  }

  void toggleLogin() {
    obscureTextLogin = !obscureTextLogin;
    notifyListeners();
  }

  void toggleSignup() {
    obscureTextSignup = !obscureTextSignup;
    notifyListeners();
  }

  void toggleSignupConfirm() {
    obscureTextSignupConfirm = !obscureTextSignupConfirm;
  }

  onDispose() {
    myFocusNodePassword.dispose();
    myFocusNodeEmail.dispose();
    myFocusNodeName.dispose();
  }

  onLogin() async {
    FocusScope.of(context).requestFocus(FocusNode());
    if (loginEmailController.text == '' || loginPasswordController.text == '') {
      ShowSnackbar.show(scaffoldKey: scaffoldKey, text: 'USERNAME PASSWORD IS REQUIRE', type: TypeSnackbar.error);
      return;
    }
    setSending = true;
    notifyListeners();
    try {
      LoginReqModel req = LoginReqModel(username: loginEmailController.text, password: loginPasswordController.text);
      var dataRes = await HttpClientService.post('auth/login', req.toJson(), hasAuthorization: false);
      if (dataRes != null) {
        LoginResModel loginResModel = LoginResModel.fromJson(dataRes);
        SharedDataLocalService.writePreferences<String>('baiwa-projext-x-token', loginResModel.accessToken);
        authProvider.setIsLogin = true;
        authProvider.setUser = loginResModel.username;
      } else {
        ShowSnackbar.show(scaffoldKey: scaffoldKey, text: 'INVALID USERNAME or PASSWORD', type: TypeSnackbar.error);
      }
    } catch (e) {
      ShowSnackbar.show(scaffoldKey: scaffoldKey, text: 'Login Fail', type: TypeSnackbar.error);
    } finally {
      setSending = false;
      notifyListeners();
    }
  }
}

class LoginReqModel {
  String username;
  String password;
  LoginReqModel({@required this.username, @required this.password});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    data['password'] = this.password;
    return data;
  }
}

class LoginResModel {
  String accessToken;
  String username;

  LoginResModel({
    this.accessToken,
    this.username,
  });

  LoginResModel.fromJson(Map<String, dynamic> json) {
    accessToken = json['access_token'];
    username = json['username'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['access_token'] = this.accessToken;
    data['username'] = this.username;
    return data;
  }
}
