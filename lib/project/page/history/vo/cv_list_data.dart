class CvListAll {
  int cvId;
  String username;
  String filename;
  String nameInServer;
  String createdBy;
  String createdDate;
  String isDeleted;

  CvListAll(
      {this.cvId,
      this.username,
      this.filename,
      this.nameInServer,
      this.createdBy,
      this.createdDate,
      this.isDeleted});

  CvListAll.fromJson(Map<String, dynamic> json) {
    cvId = json['cv_id'];
    filename = json['filename'];
    nameInServer = json['name_in_server'];
    createdBy = json['created_by'];
    createdDate = json['created_date'];
    isDeleted = json['is_deleted'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cv_id'] = this.cvId;
    data['filename'] = this.filename;
    data['name_in_server'] = this.nameInServer;
    data['created_by'] = this.createdBy;
    data['created_date'] = this.createdDate;
    data['is_deleted'] = this.isDeleted;
    return data;
  }
}
