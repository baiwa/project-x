import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:path/path.dart' as path;
import 'package:projectx/framework/service/http_client_service.dart';
import 'package:projectx/project/commons/constants/theme.dart';
import 'package:projectx/project/page/history/vo/cv_list_data.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:intl/intl.dart';

class HistoryUpload extends StatefulWidget {
  @override
  _HistoryUploadState createState() => _HistoryUploadState();
}

class _HistoryUploadState extends State<HistoryUpload> {
  // List<String> pdf = [];

  String upfilehistory = '';
  List<CvListAll> cvListAll = [];

  void chooseFileCV() async {
    // String pdfData = "pdf";
    upfilehistory = await FilePicker.getFilePath();

    setState(() {
      // pdf.add(pdfData);
    });
  }

  void clearSenthistory() {
    setState(() {
      // upfilehistory = '';
    });
  }

  Future<bool> getAllData() async {
    var res = await HttpClientService.get('cv/byname');
    cvListAll.clear();
    res.forEach((item) {
      cvListAll.add(CvListAll.fromJson(item));
    });

    return true;
  }

  void upLoadFileCV() async {
    FormData dataSent = FormData.fromMap(
      {
        "image": await MultipartFile.fromFile(
          upfilehistory,
          filename: path.basename(upfilehistory),
        )
      },
    );
    var response = await HttpClientService.post(
        "file-manager/uploads", dataSent,
        isFormData: true);

    var res = await HttpClientService.post('cv/save', {
      "filename": path.basename(upfilehistory),
      "nameInServer": response[0],
    });

    upfilehistory = '';
    setState(() {});
    // print(res);
    print(response);
  }

  void showFileNameServer(String nameInServer) async {
    // FlutterDownloader.initialize();
    // final taskId = await FlutterDownloader.enqueue(
    //   url:
    //       'https://pixabay.com/th/photos/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B9%81%E0%B8%9A%E0%B8%9A%E0%B8%97%E0%B8%B5%E0%B9%88-%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%96%E0%B9%88%E0%B8%B2%E0%B8%A2%E0%B8%A0%E0%B8%B2%E0%B8%9E-%E0%B8%84%E0%B8%A7%E0%B8%B2%E0%B8%A1%E0%B8%AB%E0%B8%A5%E0%B8%87%E0%B9%83%E0%B8%AB%E0%B8%A5-1540167/',
    //   savedDir: '/',
    //   showNotification: true,
    //   openFileFromNotification: true,
    // );
    // // FlutterDownloader.registerCallback(nameInServer);

    // FlutterDownloader.open(taskId: taskId);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        // appBar: new AppBar(
        //   title: new Text('Name here'),
        // ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: SafeArea(
                child: FutureBuilder(
                    future: getAllData(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData)
                        return SingleChildScrollView(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: cvListAll.map(
                              (item) {
                                var index = cvListAll.indexOf(item);
                                return Column(
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () {
                                        showFileNameServer(item.nameInServer);
                                      },
                                      child: Card(
                                        color: Colors.white,
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                            left: 20,
                                            right: 20,
                                          ),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: <Widget>[
                                              Icon(
                                                Icons.description,
                                                color: Colors.black38,
                                                size: 30.0,
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 15),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              bottom: 2,
                                                              top: 10),
                                                      child: Text(
                                                        item.filename,
                                                        style: TextStyle(
                                                            fontSize: 20,
                                                            color:
                                                                Colors.black54),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              bottom: 10,
                                                              top: 2),
                                                      child: Text(
                                                        new DateFormat(
                                                                'EEE, d/M/y ,HH:mm ')
                                                            .format(DateTime
                                                                .parse(item
                                                                    .createdDate)),
                                                        style: TextStyle(
                                                            fontSize: 10,
                                                            color:
                                                                Colors.black45),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                );
                              },
                            ).toList(),
                          ),
                        );
                      else
                        return SpinKitFoldingCube(
                          color: ColorsCustom.green,
                        );
                    }),
              ),
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                if (upfilehistory != '')
                  Padding(
                    padding: EdgeInsets.only(
                        bottom: 10, left: 15, right: 5, top: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          path.basename(upfilehistory) ?? '',
                        ),
                        IconButton(
                          color: Colors.white,
                          icon: Icon(Icons.cancel),
                          onPressed: () {
                            setState(() {
                              upfilehistory = '';
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                GestureDetector(
                  onTap: upfilehistory.length > 0
                      ? () {
                          if (upfilehistory != '')
                            upLoadFileCV();
                          else
                            () {
                              return "กรุณาเลือกไฟล์";
                            };
                        }
                      : () {
                          if (upfilehistory == '') chooseFileCV();
                        },
                  child: Card(
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 15, left: 15, right: 15, bottom: 15),
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.archive,
                            color: Colors.black38,
                            size: 30.0,
                          ),
                          Text(
                            upfilehistory.length > 0 ? 'Upload' : 'Choose file',
                            style:
                                TextStyle(fontSize: 20, color: Colors.black38),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ));
  }
}
