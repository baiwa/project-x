import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:projectx/project/commons/constants/theme.dart';
import 'package:projectx/project/models/leave.model.dart';
import 'package:intl/intl.dart';

class EmailShowDetail extends StatefulWidget {
  final GetLeaveModel emailList;
  EmailShowDetail({this.emailList});
  @override
  _EmailShowDetailState createState() =>
      _EmailShowDetailState(emailList: emailList);
}

class _EmailShowDetailState extends State<EmailShowDetail> {
  final GetLeaveModel emailList;
  _EmailShowDetailState({this.emailList});
  @override
  void initState() {
   
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    log(emailList.toString());
    var fileList = emailList.fileUploadName.split(",");
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorsCustom.black,
        actions: <Widget>[],
        title: Text('Leave Detail'),
      ),
      backgroundColor: Theme.of(context).backgroundColor,
      body: SafeArea(
          child: ListView(children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 100, right: 20, left: 20, bottom: 100),
          child: Card(
            color: Colors.white,
            child: Padding(
              padding:
                  EdgeInsets.only(top: 20, right: 10, left: 15, bottom: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    emailList.title,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                  Text(
                    DateFormat('dd / MM / yyyy').format(DateTime.parse(
                        emailList.createdDate)),
                    style: TextStyle(color: Colors.black),
                  ),
                  // Wrap(
                  //     children: sendtoList.map((item) {
                  //   var index = sendtoList.indexOf(item);

                  //   if (index == sendtoList.length - 1) {
                  //     return Text(
                  //       item,
                  //       style: TextStyle(color: Colors.black),
                  //     );
                  //   } else {
                  //     return Text(
                  //       item + ", ",
                  //       style: TextStyle(color: Colors.black),
                  //     );
                  //   }
                  // }).toList()),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Text(
                      emailList.detail,
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: fileList.map((item) {
                        return Text(
                          item,
                          style: TextStyle(color: Colors.black),
                        );
                      }).toList(),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ])),
    );
  }
}
