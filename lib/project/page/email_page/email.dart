
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:projectx/framework/service/http_client_service.dart';
import 'package:projectx/project/models/get_user_detail.dart';
import 'package:projectx/project/page/email_page/email_send.dart';
import 'package:projectx/project/page/email_page/email_show.dart';
import 'package:provider/provider.dart';
import 'package:projectx/project/page/leave_page/dialog.dart';
import "dart:math" as math;

class Email extends StatefulWidget {
  @override
  _EmailState createState() => _EmailState();
}

class _EmailState extends State<Email> {
  PageController _controller;
  @override
  void initState() {
    _controller = PageController(
      initialPage: EmailPvd().pageNumber,
    );
    super.initState();
  }

  @override
  void dispose() {
    // _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<EmailPvd>(
        create: (context) => EmailPvd()..getAllEmail(),
        child: Scaffold(
          body: SafeArea(
            child: PageView(
              controller: _controller,
              children: [
                EmailShow(),
                EmailSend(),
              ],
            ),
          ),
        ));
  }
}

class EmailPvd with ChangeNotifier {
  int pageNumber = 0;
  List<GetUserDetail> userData = [];
  List<Color> colors = [];
  void getAllEmail() async {
    var response = await HttpClientService.get("users/getall-dtl");
    for (var index = 0; index < response.length; index++) {
      userData.add(GetUserDetail.fromJson(response[index]));
      colors
          .add(colorListCustom[math.Random().nextInt(colorListCustom.length)]);
    }
  }
}
