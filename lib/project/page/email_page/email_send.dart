import 'dart:developer';
import 'dart:io';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:path/path.dart' as path;
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:projectx/framework/service/http_client_service.dart';
import 'package:projectx/project/commons/constants/theme.dart';
import 'package:projectx/project/models/get_user_detail.dart';
import 'package:projectx/project/models/leave.model.dart';
import 'package:projectx/project/page/email_page/email.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import 'package:dio/dio.dart';

class EmailSend extends StatefulWidget {
  @override
  _EmailSendState createState() => _EmailSendState();
}

class _EmailSendState extends State<EmailSend> {
  TextEditingController sendToController = new TextEditingController();
  TextEditingController titleLeaveController = new TextEditingController();
  TextEditingController detailController = new TextEditingController();
  TextEditingController addEmailController = new TextEditingController();

  var backupDetail = '';
  List<File> files = [];
  List<File> filesImage = [];
  List<GetUserDetail> userData = [];
  List<GetUserDetail> selectedEmailforShow = [];
  List<String> selectedEmailforSent = [];
  String dropdownValue = 'Leave Type';
  String dropdownDisableValue = 'Leave Type';
  DateTime startDate;
  DateTime endDate = new DateTime.now();
  List<Color> colors = [];
  List<Color> colorsBackup = [];
  bool isOneDay = true;

  @override
  void dispose() {
    // clearSent();
    sendToController.dispose();
    titleLeaveController.dispose();
    detailController.dispose();
    addEmailController.dispose();

    super.dispose();
  }

  void addCustomEmail() {
    setState(() {
      var sent = GetUserDetail(
          email: addEmailController.text,
          imgProfile:
              "https://www.wisible.io/wp-content/uploads/2019/08/avatar-human-male-profile-user-icon-518358.png",
          username: "");
      selectedEmailforSent.add(sent.email);
      selectedEmailforShow.add(sent);
    });
  }

  void clearSent() {
    setState(() {
      dropdownValue = dropdownDisableValue;
      selectedEmailforSent.clear();
      selectedEmailforShow.clear();
      addEmailController.clear();
      detailController.clear();
      sendToController.clear();
      titleLeaveController.clear();
      files.clear();
      startDate = null;
      endDate = null;
      // userData = [];
    });
  }

  void filesPick() async {
    detailController.text = backupDetail;
    detailController = detailController;
    var res = await FilePicker.getMultiFile();
    setState(() {
      files.addAll(res);
    });
  }

  void removeFilesPick(index) async {
    setState(() {
      files.removeAt(index);
    });
  }

  void backupDetailController(text) {
    backupDetail = text;
  }

  void postFileEmail(
      List<String> sendTo,
      String title,
      String detail,
      List<File> files,
      String leaveType,
      DateTime startDate,
      DateTime endDate) async {
    try {
      final Map<String, dynamic> dataImage = Map<String, dynamic>();
      final urlUploadFile = "file-manager/uploads";
      final urlSent = "leave/save";
      var dateType = '';
      List<String> imageName = [];
      List<String> imageOriginalName = [];
      if (true == isOneDay) {
        dateType = 'One Day';
      } else {
        dateType = 'Many Days';
      }
      if (0 < files.length) {
        for (var index = 0; index < files.length; index++) {
          dataImage['image' + index.toString()] = await MultipartFile.fromFile(
            files[index].path,
            filename: path.basename(files[index].path),
          );
          imageOriginalName.add(path.basename(files[index].path));
        }
        FormData dataImageSent = FormData.fromMap(dataImage);
        HttpClientService.post(urlUploadFile, dataImageSent, isFormData: true)
            .then((res) {
          log(res.toString());
          for (var index = 0; index < res.length; index++) {
            imageName.add(res[index]);
            if (index == res.length - 1) {
              var dataForSend = SendLeaveModel(
                  title: title,
                  sendTo: sendTo,
                  detail: detail,
                  endDate: endDate.toString(),
                  fileUploadName: imageOriginalName,
                  fileUpload: imageName,
                  leaveType: leaveType,
                  startDate: startDate.toString(),
                  dateType: dateType);

              postData(urlSent, dataForSend);
            }
          }
        });
      } else {
        var dataForSend = SendLeaveModel(
            title: title,
            sendTo: sendTo,
            detail: detail,
            endDate: endDate.toString(),
            // fileUpload: imageName,
            leaveType: leaveType,
            startDate: startDate.toString(),
            dateType: dateType);

        postData(urlSent, dataForSend);
      }
    } catch (error) {
      Scaffold.of(context).showSnackBar(SnackBar(
        backgroundColor: Colors.yellow,
        content: Text(
          error.toString(),
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white),
        ),
      ));
    }
  }

  void postData(urlSent, dataForSend) async {
    try {
      var res =
          await HttpClientService.post(urlSent, dataForSend, isFormData: true);
      Scaffold.of(context).showSnackBar(SnackBar(
        backgroundColor: Colors.green,
        content: Text(
          "Send!",
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white),
        ),
      ));
    } catch (error) {
      Scaffold.of(context).showSnackBar(SnackBar(
        backgroundColor: Colors.yellow,
        content: Text(
          error.toString(),
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white),
        ),
      ));
    }
    clearSent();
  }

  @override
  Widget build(BuildContext context) {
    final myModel = Provider.of<EmailPvd>(context, listen: false);
    if (selectedEmailforShow.length == 0 && selectedEmailforSent.length == 0) {
      userData = myModel.userData.toSet().toList();
      colors = myModel.colors.toSet().toList();
    }
    void showDialogEmail() {
      showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, setState) {
              return Dialog(
                backgroundColor: Colors.transparent,
                child: SingleChildScrollView(
                  child: (Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 0, bottom: 50),
                        child: Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0)),
                          color: Colors.white,
                          child: Padding(
                              padding: EdgeInsets.only(left: 20),
                              child: Row(
                                children: <Widget>[
                                  Flexible(
                                    child: TextField(
                                      cursorColor: Colors.pink,
                                      controller: addEmailController,
                                      keyboardType: TextInputType.emailAddress,
                                      style: TextStyle(color: Colors.black),
                                      decoration: InputDecoration(
                                        counterStyle:
                                            TextStyle(color: Colors.grey),
                                        border: InputBorder.none,
                                        icon: Icon(
                                          Icons.email,
                                          color: Color(0xFF263147),
                                        ),
                                        hintText: "Email",
                                        hintStyle:
                                            TextStyle(color: Colors.grey),
                                      ),
                                    ),
                                  ),
                                  OutlineButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(30.0)),
                                      onPressed:
                                          addEmailController.text.length > 6
                                              ? () {
                                                  setState(() {
                                                    addCustomEmail();
                                                    addEmailController.clear();
                                                  });
                                                }
                                              : () {
                                                  setState(() {});
                                                },
                                      child: Text("add",
                                          style: TextStyle(
                                              color: ColorsCustom.black)))
                                ],
                              )),
                        ),
                      ),
                      Column(
                          mainAxisSize: MainAxisSize.max,
                          children: selectedEmailforShow.map((item) {
                            var index = selectedEmailforShow.indexOf(item);
                            return (Card(
                              shape: RoundedRectangleBorder(
                                side:
                                    BorderSide(color: Colors.green, width: 3.0),
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              color: Colors.white,
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    width: 50.0,
                                    height: 50.0,
                                    decoration: new BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: new DecorationImage(
                                        fit: BoxFit.cover,
                                        image: new NetworkImage(
                                            HttpClientService.contextPath +
                                                'file-manager/get-img/' +
                                                item.imgProfile +
                                                "?height=50&width=50"),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding:
                                          EdgeInsets.only(right: 20, left: 10),
                                      child: Text(
                                        item.email,
                                        style:
                                            TextStyle(color: Color(0xFF263147)),
                                      ),
                                    ),
                                  ),
                                  IconButton(
                                      color: Colors.red,
                                      icon: Icon(Icons.remove),
                                      onPressed: () {
                                        userData.add(item);
                                        colors.add(colorsBackup[index]);
                                        colorsBackup.removeAt(index);
                                        selectedEmailforShow.removeAt(index);
                                        selectedEmailforSent.removeAt(index);
                                        setState(() {});
                                      }),
                                ],
                              ),
                            ));
                          }).toList()),
                      Divider(),
                      Column(
                        children: userData.map(
                          (item) {
                            var index = userData.indexOf(item);
                            return (Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(45.0),
                              ),
                              color: colors[index],
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    width: 50.0,
                                    height: 50.0,
                                    decoration: new BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: new DecorationImage(
                                        fit: BoxFit.cover,
                                        image: new NetworkImage(
                                            HttpClientService.contextPath +
                                                'file-manager/get-img/' +
                                                item.imgProfile),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding:
                                          EdgeInsets.only(right: 20, left: 10),
                                      child: Text(
                                        item.email,
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                  ),
                                  IconButton(
                                    color: Colors.white,
                                    icon: Icon(Icons.add),
                                    onPressed: () {
                                      setState(() {
                                        colorsBackup.add(colors[index]);
                                        colors.removeAt(index);
                                        userData.removeAt(index);
                                        selectedEmailforShow.add(item);
                                        selectedEmailforSent.add(item.username);
                                        setState(() {});
                                      });
                                    },
                                  ),
                                ],
                              ),
                            ));
                          },
                        ).toList(),
                      ),
                    ],
                  )),
                ),
              );
            },
          );
        },
      );
    }

    log('--->' + myModel.pageNumber.toString());
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SafeArea(
        child: ListView(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(top: 80, right: 10, left: 10),
                child: Column(
                  children: <Widget>[
                    Card(
                        color: Colors.white,
                        child: Padding(
                          padding:
                              EdgeInsets.only(top: 20, right: 20, left: 20),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Row(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding:
                                        EdgeInsets.only(right: 16, left: 5),
                                    child: Text(
                                      "To:",
                                      style: TextStyle(
                                          color: Color(0xFF263147),
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        showDialogEmail();
                                      },
                                      child: selectedEmailforSent.length == 0
                                          ? Text(
                                              "Send to Email",
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontWeight: FontWeight.bold),
                                            )
                                          : Wrap(
                                              children: selectedEmailforShow
                                                  .map((item) {
                                              var index = selectedEmailforShow
                                                  .indexOf(item);
                                              return (Card(
                                                color: colorsBackup[index],
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            30.0)),
                                                child: Padding(
                                                  padding: EdgeInsets.only(
                                                      right: 5,
                                                      left: 5,
                                                      top: 5,
                                                      bottom: 5),
                                                  child: Text(
                                                    item.email,
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                ),
                                              ));
                                            }).toList()),
                                    ),
                                  ),
                                ],
                              ),
                              TextField(
                                cursorColor: Colors.pink,
                                controller: titleLeaveController,
                                keyboardType: TextInputType.emailAddress,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  icon: Icon(
                                    Icons.title,
                                    color: Color(0xFF263147),
                                  ),
                                  hintText: "Title",
                                  hintStyle: TextStyle(color: Colors.grey),
                                ),
                              ),
                            ],
                          ),
                        ))
                  ],
                )),
            Padding(
              padding: EdgeInsets.all(10),
              child: Card(
                color: Colors.white,
                child: Padding(
                  padding:
                      EdgeInsets.only(top: 10, right: 20, left: 20, bottom: 20),
                  child:
                      Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
                    SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: Theme(
                        data: Theme.of(context).copyWith(
                          canvasColor: Colors.white,
                        ),
                        child: DropdownButton<String>(
                          value: dropdownValue,
                          icon: Icon(
                            Icons.arrow_downward,
                            color: ColorsCustom.black,
                          ),
                          isExpanded: true,
                          style: TextStyle(color: ColorsCustom.black),
                          underline: Container(
                            height: 2,
                            color: ColorsCustom.black,
                          ),
                          onChanged: (String newValue) {
                            if (newValue != dropdownDisableValue)
                              setState(() {
                                dropdownValue = newValue;
                              });
                          },
                          items: <String>[
                            dropdownDisableValue,
                            'ลาป่วย',
                            'ลากิจ',
                            'ลาพักร้อน'
                          ].map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                                value: value,
                                child: value == dropdownDisableValue
                                    ? Text(
                                        value,
                                        style: TextStyle(color: Colors.grey),
                                      )
                                    : Text(value));
                          }).toList(),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: isOneDay
                              ? <Widget>[
                                  Expanded(
                                    child: RaisedButton(
                                      splashColor: Colors.black,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(30),
                                              bottomLeft: Radius.circular(30))),
                                      onPressed: () {},
                                      color: ColorsCustom.green,
                                      child: Text(
                                        "One Day",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: OutlineButton(
                                      color: Colors.white,
                                      onPressed: () {
                                        setState(() {
                                          isOneDay = false;
                                        });
                                      },
                                      child: Text(
                                        "Many Days",
                                        style: TextStyle(color: Colors.black),
                                      ),
                                    ),
                                  )
                                ]
                              : <Widget>[
                                  Expanded(
                                    child: OutlineButton(
                                      onPressed: () {
                                        setState(() {
                                          isOneDay = true;
                                        });
                                      },
                                      color: Colors.white,
                                      child: Text(
                                        "One Day",
                                        style: TextStyle(
                                            color: ColorsCustom.black),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: RaisedButton(
                                      splashColor: Colors.black,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(30),
                                              bottomRight:
                                                  Radius.circular(30))),
                                      color: ColorsCustom.green,
                                      onPressed: () {},
                                      child: Text(
                                        "Many Days",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                  )
                                ]),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    SizedBox(
                        child: isOneDay
                            ? FlatButton(
                                onPressed: () {
                                  DatePicker.showDatePicker(context,
                                      showTitleActions: true,
                                      minTime: DateTime.now(),
                                      maxTime: DateTime(2022, 12, 31),
                                      onChanged: (date) {
                                    print('change $date');
                                    startDate = date;
                                  }, onConfirm: (date) {
                                    print('confirm $date');
                                    setState(() {
                                      startDate = date;
                                    });
                                  },
                                      currentTime: startDate,
                                      locale: LocaleType.th);
                                },
                                child: Text(
                                  null != startDate
                                      ? DateFormat('dd / MM / yyyy')
                                          .format(startDate)
                                      : 'Select Date',
                                  style: TextStyle(color: ColorsCustom.black),
                                ))
                            : Wrap(
                                crossAxisAlignment: WrapCrossAlignment.center,
                                children: <Widget>[
                                  FlatButton(
                                      onPressed: () {
                                        DatePicker.showDatePicker(context,
                                            showTitleActions: true,
                                            minTime: DateTime.now(),
                                            maxTime: DateTime(2022, 12, 31),
                                            onChanged: (date) {
                                          print('change $date');
                                          startDate = date;
                                        }, onConfirm: (date) {
                                          print('confirm $date');
                                          setState(() {
                                            startDate = date;
                                          });
                                        },
                                            currentTime: startDate,
                                            locale: LocaleType.th);
                                      },
                                      child: Text(
                                        null != startDate
                                            ? DateFormat('dd / MM / yyyy')
                                                .format(startDate)
                                            : 'Start Date',
                                        style: TextStyle(
                                            color: ColorsCustom.black),
                                      )),
                                  SizedBox(
                                      child: null != endDate
                                          ? Text(
                                              "-",
                                              style: TextStyle(
                                                  color: ColorsCustom.black),
                                            )
                                          : null),
                                  FlatButton(
                                      onPressed: null != startDate
                                          ? () {
                                              DatePicker.showDatePicker(context,
                                                  showTitleActions: true,
                                                  minTime: startDate,
                                                  maxTime:
                                                      DateTime(2022, 12, 31),
                                                  onChanged: (date) {
                                                print('change $date');
                                                endDate = date;
                                              }, onConfirm: (date) {
                                                print('confirm $date');
                                                setState(() {
                                                  endDate = date;
                                                });
                                              },
                                                  currentTime: startDate,
                                                  locale: LocaleType.th);
                                            }
                                          : null,
                                      child: null != startDate
                                          ? Text(
                                              null != endDate
                                                  ? DateFormat('dd / MM / yyyy')
                                                      .format(endDate)
                                                  : 'End Date',
                                              style: TextStyle(
                                                  color: ColorsCustom.black),
                                            )
                                          : Text('End Date',
                                              style: TextStyle(
                                                  color: Colors.grey)))
                                ],
                              ))
                  ]),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(right: 10, left: 10),
              child: Card(
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.only(top: 20, right: 20, left: 20),
                  child:
                      Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                    TextField(
                      cursorColor: Colors.pink,
                      maxLength: 250,
                      maxLines: 6,
                      minLines: 3,
                      onChanged: (text) {
                        backupDetailController(text);
                      },
                      controller: detailController,
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        counterStyle: TextStyle(color: Colors.grey),
                        border: InputBorder.none,
                        enabledBorder: const OutlineInputBorder(
                          borderSide:
                              const BorderSide(color: Colors.grey, width: 0.0),
                        ),
                        icon: Icon(
                          Icons.textsms,
                          color: Color(0xFF263147),
                        ),
                        hintText: "Detail",
                        hintStyle: TextStyle(color: Colors.grey),
                      ),
                    ),
                    Divider(),
                    IconButton(
                        color: Color(0xFF263147),
                        icon: Icon(Icons.attach_file),
                        onPressed: () {
                          filesPick();
                        }),
                    Padding(
                        padding: EdgeInsets.only(bottom: 20),
                        child: null != files
                            ? Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: files.map((item) {
                                  var index = files.indexOf(item);
                                  return (Row(
                                    // crossAxisAlignment:
                                    //     CrossAxisAlignment.stretch,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Text(
                                        path.basename(item.path),
                                        style: TextStyle(
                                            color: ColorsCustom.black),
                                      ),
                                      IconButton(
                                          color: Colors.red,
                                          icon: Icon(Icons.close),
                                          onPressed: () {
                                            removeFilesPick(index);
                                          }),
                                    ],
                                  ));
                                }).toList())
                            : null)
                  ]),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 100),
              child: Container(
                margin: EdgeInsets.only(top: 30.0, right: 50, left: 50),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Color(0xFF54688d),
                      offset: Offset(1.0, 6.0),
                      blurRadius: 20.0,
                    ),
                    BoxShadow(
                      color: Color(0xFF1d2637),
                      offset: Offset(1.0, 6.0),
                      blurRadius: 20.0,
                    ),
                  ],
                  gradient: LinearGradient(
                      colors: [Color(0xFF26b851), Color(0xFF72de92)],
                      begin: const FractionalOffset(0.2, 0.2),
                      end: const FractionalOffset(1.0, 1.0),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp),
                ),
                child: MaterialButton(
                    highlightColor: Colors.transparent,
                    splashColor: Color(0xFF1d2637),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 42.0),
                      child: Text(
                        "SEND",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 25.0,
                            fontFamily: "WorkSansBold"),
                      ),
                    ),
                    onPressed: () {
                      if (null == endDate) {
                        endDate = startDate;
                      }
                      if (0 != titleLeaveController.text.length &&
                          0 != selectedEmailforSent.length &&
                          dropdownDisableValue != dropdownValue &&
                          null != startDate) {
                        if (endDate.isAfter(startDate) ||
                            0 == endDate.compareTo(startDate)) {
                          postFileEmail(
                              selectedEmailforSent,
                              titleLeaveController.text,
                              detailController.text,
                              files,
                              dropdownValue,
                              startDate,
                              endDate);
                        } else {
                          if (true == isOneDay) {
                            endDate = startDate;
                            postFileEmail(
                                selectedEmailforSent,
                                titleLeaveController.text,
                                detailController.text,
                                files,
                                dropdownValue,
                                startDate,
                                endDate);
                          } else {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              backgroundColor: Colors.yellow,
                              content: Text(
                                "Warning: Check your start date and end date.",
                                textAlign: TextAlign.center,
                              ),
                            ));
                          }
                        }
                      } else {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          backgroundColor: Colors.yellow,
                          content: Text(
                            "Warning!",
                            textAlign: TextAlign.center,
                          ),
                        ));
                      }
                    }),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
