import 'dart:developer';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:projectx/framework/service/http_client_service.dart';
import 'package:projectx/project/commons/constants/theme.dart';
import 'package:projectx/project/commons/constants/user_rule.dart';
import 'package:projectx/project/commons/global_var.dart';
import 'package:projectx/project/commons/service/go_location_service.dart';
import 'package:projectx/project/models/leave.model.dart';
import 'package:projectx/project/page/email_page/email.dart';
import 'package:projectx/project/page/email_page/email_show_datail.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class EmailShow extends StatefulWidget {
  @override
  _EmailShowState createState() => _EmailShowState();
}

class _EmailShowState extends State<EmailShow> {
  PageController _controller;
  List<GetLeaveModel> emailList = [];
  List<GetLeaveModel> emailAllList = [];
  static String showall = 'All';
  String showFitter = new DateTime.now().year.toString();
  int countFitter = 0;
  int currentPageNumber = 0;
  @override
  void initState() {
    if (0 == emailList.length) {
      getListEmail();
    }
    if (userRuleAdmin.toLowerCase() == dataUserDetail.userRule.toLowerCase()) {
      getListAllEmail();
    }
    _controller = PageController(
      initialPage: currentPageNumber,
    );
    super.initState();
  }

  @override
  void dispose() {
    // emailList.clear();
    super.dispose();
  }

  void getListEmail() async {
    emailList.clear();
    log("get List EMail");
    var response = await HttpClientService.get("leave/getall");
    for (var index = 0; index < response.length; index++) {
      // log(response[index].toString());
      if (null != response) {
        setState(() {
          emailList.add(GetLeaveModel.fromJson(response[index]));
        });
      }
    }
  }

  void currentPage(int index) {
    // setState(() {
    //   currentPageNumber = index;
    // });
  }

  void getListAllEmail() async {
    emailAllList.clear();
    log("get List EMail");
    var response = await HttpClientService.get("leave/getall-all");
    for (var index = 0; index < response.length; index++) {
      // log(response[index].toString());
      if (null != response) {
        setState(() {
          emailAllList.add(GetLeaveModel.fromJson(response[index]));
        });
        log(response[index].toString());
      }
    }
  }

  Widget pageShowUser(BuildContext context) {
    return Column(
      children: <Widget>[
        if (userRuleAdmin.toLowerCase() ==
            dataUserDetail.userRule.toLowerCase())
          Divider(
              endIndent: MediaQuery.of(context).size.width * 0.5,
              color: Colors.orange,
              thickness: 3),
        Expanded(
            child: null != emailList
                ? ListView.builder(
                    itemCount: emailList.length,
                    itemBuilder: (BuildContext context, int index) {
                      if (showall == showFitter) {
                        countFitter = emailList.length;
                        // log(index.toString());
                        return (Padding(
                          padding: EdgeInsets.only(
                              left: 15, right: 15, bottom: 0, top: 5),
                          child: Card(
                            color: Colors.white,
                            child: GestureDetector(
                              onTap: () {
                                GoLocationService.navigatorPush(
                                    context,
                                    EmailShowDetail(
                                      emailList: emailList[index],
                                    ));
                              },
                              child: ListTile(
                                title: Text(
                                  emailList[index].title,
                                  style: TextStyle(color: ColorsCustom.black),
                                ),
                                subtitle: Text(
                                  DateFormat('dd-MM-yyyy ').format(
                                      DateTime.parse(
                                          emailList[index].createdDate)),
                                  style: TextStyle(color: ColorsCustom.black),
                                ),
                              ),
                            ),
                          ),
                        ));
                      } else {
                        if (DateTime.parse(emailList[index].createdDate)
                                .year
                                .toString() ==
                            showFitter) {
                          countFitter += index;
                          return (Padding(
                            padding: EdgeInsets.only(
                                left: 15, right: 15, bottom: 0, top: 5),
                            child: Card(
                              color: Colors.white,
                              child: GestureDetector(
                                onTap: () {
                                  GoLocationService.navigatorPush(
                                      context,
                                      EmailShowDetail(
                                        emailList: emailList[index],
                                      ));
                                },
                                child: ListTile(
                                  title: Text(
                                    emailList[index].title,
                                    style: TextStyle(color: ColorsCustom.black),
                                  ),
                                  subtitle: Text(
                                    DateFormat('dd-MM-yyyy').format(
                                        DateTime.parse(
                                            emailList[index].createdDate)),
                                    style: TextStyle(color: ColorsCustom.black),
                                  ),
                                ),
                              ),
                            ),
                          ));
                        } else {
                          return null;
                        }
                      }
                    })
                : null),
      ],
    );
  }

  Widget pageShowAdmin(BuildContext context) {
    return Column(
      children: <Widget>[
        if (userRuleAdmin.toLowerCase() ==
            dataUserDetail.userRule.toLowerCase())
          Divider(
            indent: MediaQuery.of(context).size.width * 0.5,
            thickness: 3,
            color: Colors.orange,
          ),
        Expanded(
            child: null != emailAllList
                ? ListView.builder(
                    itemCount: emailAllList.length,
                    itemBuilder: (BuildContext context, int index) {
                      if (showall == showFitter) {
                        countFitter = emailAllList.length;
                        // log(index.toString());
                        return (Padding(
                          padding: EdgeInsets.only(
                              left: 15, right: 15, bottom: 0, top: 5),
                          child: Card(
                            color: Colors.white,
                            child: GestureDetector(
                              onTap: () {
                                GoLocationService.navigatorPush(
                                    context,
                                    EmailShowDetail(
                                      emailList: emailAllList[index],
                                    ));
                              },
                              child: ListTile(
                                title: Text(
                                  emailAllList[index].title,
                                  style: TextStyle(color: ColorsCustom.black),
                                ),
                                subtitle: Text(
                                  DateFormat('dd-MM-yyyy ').format(
                                      DateTime.parse(
                                          emailAllList[index].createdDate)),
                                  style: TextStyle(color: ColorsCustom.black),
                                ),
                              ),
                            ),
                          ),
                        ));
                      } else {
                        if (DateTime.parse(emailAllList[index].createdDate)
                                .year
                                .toString() ==
                            showFitter) {
                          countFitter += index;
                          return (Padding(
                            padding: EdgeInsets.only(
                                left: 15, right: 15, bottom: 0, top: 5),
                            child: Card(
                              color: Colors.white,
                              child: GestureDetector(
                                onTap: () {
                                  GoLocationService.navigatorPush(
                                      context,
                                      EmailShowDetail(
                                        emailList: emailAllList[index],
                                      ));
                                },
                                child: ListTile(
                                  title: Text(
                                    emailAllList[index].title,
                                    style: TextStyle(color: ColorsCustom.black),
                                  ),
                                  subtitle: Text(
                                    DateFormat('dd-MM-yyyy').format(
                                        DateTime.parse(
                                            emailAllList[index].createdDate)),
                                    style: TextStyle(color: ColorsCustom.black),
                                  ),
                                ),
                              ),
                            ),
                          ));
                        } else {
                          return null;
                        }
                      }
                    })
                : null),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    Provider.of<EmailPvd>(context, listen: false);

    return ChangeNotifierProvider<EmailPvd>(
      create: (context) => EmailPvd(),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: ColorsCustom.black,
          actions: <Widget>[
            PopupMenuButton(
                icon: Icon(
                  Icons.date_range,
                ),
                onSelected: (result) {
                  switch (result) {
                    case "showAll":
                      setState(() {
                        showFitter = showall;
                      });
                      // log(showFitter);
                      break;
                    case "2019":
                      setState(() {
                        showFitter = '2019';
                      });
                      break;
                    case "2020":
                      setState(() {
                        showFitter = '2020';
                      });
                      break;
                    case "2021":
                      setState(() {
                        showFitter = '2021';
                      });
                      break;
                    case "2022":
                      setState(() {
                        showFitter = '2022';
                      });
                      break;
                    default:
                      showFitter = '';
                  }
                },
                itemBuilder: (context) => [
                      PopupMenuItem(value: "showAll", child: Text("All")),
                      PopupMenuItem(value: "2019", child: Text("2019")),
                      PopupMenuItem(value: "2020", child: Text("2020")),
                      PopupMenuItem(value: "2021", child: Text("2021")),
                      PopupMenuItem(value: "2022", child: Text("2022")),
                    ])
          ],
          title: Text('Notification'),
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        body: SafeArea(
            child: Column(children: <Widget>[
          //  if(dataUserDetail)

          Padding(
              padding: EdgeInsets.only(top: 30, bottom: 0, left: 30),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      // width: MediaQuery.of(context).size.width * 0.4,
                      child: GestureDetector(
                    onTap: () {
                      if (userRuleAdmin.toLowerCase() ==
                          dataUserDetail.userRule.toLowerCase()) {
                        _controller.animateToPage(
                          0,
                          duration: Duration(milliseconds: 600),
                          curve: Curves.fastOutSlowIn,
                        );
                      }
                    },
                    child: Row(
                      children: <Widget>[
                        Text(showFitter),
                        Padding(
                            padding: EdgeInsets.only(left: 10, right: 10),
                            child: userRuleAdmin.toLowerCase() !=
                                    dataUserDetail.userRule.toLowerCase()
                                ? Chip(
                                    labelPadding:
                                        EdgeInsets.only(left: 15, right: 15),
                                    label: Text(emailList.length.toString()),
                                    backgroundColor: Colors.orange,
                                  )
                                : Chip(
                                    labelPadding:
                                        EdgeInsets.only(left: 15, right: 15),
                                    label: Text(emailList.length.toString()),
                                    backgroundColor: Colors.grey,
                                  ))
                      ],
                    ),
                  )),
                  SizedBox(
                      width: MediaQuery.of(context).size.width * 0.4,
                      child: userRuleAdmin.toLowerCase() ==
                              dataUserDetail.userRule.toLowerCase()
                          ? GestureDetector(
                              onTap: () {
                                _controller.animateToPage(
                                  1,
                                  duration: Duration(milliseconds: 600),
                                  curve: Curves.fastOutSlowIn,
                                );
                              },
                              child: Row(
                                children: <Widget>[
                                  Text("Admin"),
                                  Padding(
                                      padding:
                                          EdgeInsets.only(left: 10, right: 10),
                                      child: Chip(
                                        backgroundColor: Colors.grey,
                                        labelPadding: EdgeInsets.only(
                                            left: 15, right: 15),
                                        label: Text(
                                            emailAllList.length.toString()),
                                      ))
                                ],
                              ))
                          : null)
                ],
              )),
          Expanded(
              child: userRuleAdmin.toLowerCase() ==
                      dataUserDetail.userRule.toLowerCase()
                  ? PageView(
                      onPageChanged: (index) => currentPage(index),
                      controller: _controller,
                      children: <Widget>[
                        pageShowUser(context),
                        pageShowAdmin(context)
                      ],
                    )
                  : pageShowUser(context)),
        ])),
      ),
    );
  }
}
