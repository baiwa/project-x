import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:projectx/project/commons/constants/theme.dart';
import 'package:projectx/project/page/bottom_bar/bottombar_wg.dart';

List<BottomNavyBarItem> listMenu = <BottomNavyBarItem>[
  BottomNavyBarItem(
    title: Text('HOME', style: TextStyle(fontWeight: FontWeight.w100)),
    activeColor: ColorsCustom.green,
    child: SvgPicture.asset(
      'assets/images/bottombar_icon/home.svg',
      color: ColorsCustom.green,
    ),
  ),
  BottomNavyBarItem(
    title: Text('FARE', style: TextStyle(fontWeight: FontWeight.w100)),
    activeColor: ColorsCustom.green,
    child: SvgPicture.asset(
      'assets/images/bottombar_icon/car.svg',
      color: ColorsCustom.green,
      height: 23,
    ),
  ),
  BottomNavyBarItem(
    title: Text('News', style: TextStyle(fontWeight: FontWeight.w100)),
    activeColor: ColorsCustom.green,
    child: SvgPicture.asset(
      'assets/images/bottombar_icon/book.svg',
      color: ColorsCustom.green,
    ),

    // child: Badge(
    //   badgeColor: Colors.red,
    //   position: BadgePosition.topRight(right: -2),
    //   badgeContent: Text(
    //     '9',
    //     style: TextStyle(
    //       color: Colors.white,
    //       fontSize: 8,
    //     ),
    //   ),
    //   child: SvgPicture.asset(
    //     'assets/images/bottombar_icon/cart.svg',
    //     color: ColorsCustom.green,
    //   ),
    // ),
  ),
  BottomNavyBarItem(
    title: Text('Leave', style: TextStyle(fontWeight: FontWeight.w100)),
    activeColor: ColorsCustom.green,
    child: SvgPicture.asset(
      'assets/images/bottombar_icon/calendar-full.svg',
      color: ColorsCustom.green,
    ),
  ),
  BottomNavyBarItem(
    title: Text('CV', style: TextStyle(fontWeight: FontWeight.w100)),
    activeColor: ColorsCustom.green,
    child: SvgPicture.asset(
      'assets/images/bottombar_icon/upload.svg',
      color: ColorsCustom.green,
    ),
  ),
  BottomNavyBarItem(
    title: Text('SETTING', style: TextStyle(fontWeight: FontWeight.w100)),
    activeColor: ColorsCustom.green,
    child: SvgPicture.asset(
      'assets/images/bottombar_icon/cog.svg',
      color: ColorsCustom.green,
    ),
  ),
];
