import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:projectx/project/page/setting/orther_wg/setting_wg.dart';
import 'package:projectx/project/page/setting/orther_wg/user_detail_ui.dart';
import 'package:projectx/project/page/setting/user_edit_detail_page/user_edit_detail_page.dart';
import 'package:projectx/project/provider/authen_pvd.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:projectx/project/commons/constants/theme.dart';
import 'package:projectx/project/commons/i18n/keys.dart';
import 'package:projectx/project/commons/service/go_location_service.dart';
import 'package:projectx/project/commons/utils/theme.dart';
import 'package:projectx/project/page/setting/lang_setting/lang_setting_page.dart';
import 'package:xlive_switch/xlive_switch.dart';

class SettingListWg extends StatefulWidget {
  SettingListWg({Key key}) : super(key: key);
  @override
  _SettingListWgState createState() => _SettingListWgState();
}

class _SettingListWgState extends State<SettingListWg> {
  bool isSwitched = true;
  bool darkThemeOn = false;

  void onThemeChanged(bool value, ThemeNotifier themeNotifier) async {
    (value)
        ? themeNotifier.setTheme(darkTheme)
        : themeNotifier.setTheme(lightTheme);
    var prefs = await SharedPreferences.getInstance();
    prefs.setBool('darkMode', value);
  }

  @override
  void initState() {
    SharedPreferences.getInstance().then((prefs) {
      var darkModeOn = prefs.getBool('darkMode') ?? false;
      darkThemeOn = darkModeOn;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final themeNotifier = Provider.of<ThemeNotifier>(context);
    darkThemeOn = (themeNotifier.getTheme() == darkTheme);

    return Scaffold(
        backgroundColor: ColorsCustom.black,
        body: Column(
          children: <Widget>[
            UserDetail(),
            Expanded(
              child: ListView(
                children: <Widget>[
                  TextHederListSetting('User Detail'),
                  ListSetting(
                    text: 'User Detail',
                    icon: Icons.person,
                    action: ForwardArrowSetting(),
                    onTab: () async {
                      GoLocationService.navigatorPush(
                        context,
                        UserEditDetail(),
                      );
                    },
                  ),
                  TextHederListSetting('General Setting'),
                  ListSetting(
                    text: 'Dark Theme',
                    icon: Icons.brightness_4,
                    action: XlivSwitch(
                      value: darkThemeOn,
                      onChanged: (value) {
                        // setState(() {
                        //   darkThemeOn = value;
                        // });
                        // onThemeChanged(value, themeNotifier);
                      },
                    ),
                  ),
                  ListSetting(
                    text: 'Notify',
                    icon: Icons.notifications_none,
                    action: XlivSwitch(
                      value: isSwitched,
                      onChanged: (value) {
                        setState(() {
                          isSwitched = value;
                        });
                      },
                    ),
                  ),
                  ListSetting(
                    text: translate(Keys.Setting_Page_Languages_Setting_Title),
                    icon: Icons.g_translate,
                    action: ForwardArrowSetting(),
                    onTab: () {
                      GoLocationService.navigatorPush(
                        context,
                        LangSettingPage(),
                      );
                    },
                  ),
                  ListSetting(
                    text: 'Info Detail',
                    icon: Icons.info,
                    action: ForwardArrowSetting(),
                  ),
                  ListSetting(
                    text: 'SIGHT OUT',
                    icon: Icons.exit_to_app,
                    action: ForwardArrowSetting(),
                    color: Colors.red,
                    onTab: () {
                      AuthenPvd authProvider =
                          Provider.of<AuthenPvd>(context, listen: false);
                      authProvider.setIsLogin = false;
                    },
                  ),
                  SizedBox(height: 10),
                ],
              ),
            ),
          ],
        ));
  }
}
