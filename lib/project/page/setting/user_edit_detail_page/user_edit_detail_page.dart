import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:projectx/project/commons/constants/theme.dart';
import 'package:projectx/project/commons/global_var.dart';
import 'package:projectx/project/models/get_user_detail.dart';

class UserEditDetail extends StatefulWidget {
  @override
  _UserEditDetailState createState() => _UserEditDetailState();
}

class _UserEditDetailState extends State<UserEditDetail> {
  bool isEditFname = true;
  GetUserDetail userData = dataUserDetail;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // title: Text(
        //   "User Detail",
        // ),
        centerTitle: true,
        automaticallyImplyLeading: true,
      ),
      backgroundColor: ColorsCustom.black,
      body: SafeArea(
          child: ListView(
        children: <Widget>[
          Padding(
              padding:
                  EdgeInsets.only(top: 50, bottom: 50, right: 20, left: 20),
              child: Card(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.center,
                        child: Text(
                          "User Detail",
                          style: TextStyle(
                              color: ColorsCustom.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 24),
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                        "First name: ",
                        style: TextStyle(color: ColorsCustom.black),
                      ),
                          if (!isEditFname)
                            Padding(
                                padding: EdgeInsets.only(top: 20),
                                child: Text(
                                  userData.firstName,
                                  style: TextStyle(color: ColorsCustom.black),
                                )),
                          if (isEditFname)
                            Flexible(
                                child: TextField(
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                // icon: Icon(
                                //   Icons.title,
                                //   color: Color(0xFF263147),
                                // ),
                                hintText: userData.firstName,
                                hintStyle: TextStyle(color: ColorsCustom.black,fontSize: 18),
                              ),
                              // userData.firstName,
                              // style: TextStyle(color: Colors.white),
                            )),
                          IconButton(
                            onPressed: () {
                              setState(() {
                                // isEditFname = isEditFname == isEditFname;
                                log(isEditFname.toString());
                              });
                            },
                            color: Colors.orange,
                            icon: Icon(Icons.edit),
                          )
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ))
        ],
      )),
    );
  }
}
