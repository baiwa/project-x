import 'package:flutter/material.dart';
import 'package:projectx/project/commons/constants/theme.dart';

class ListSetting extends StatelessWidget {
  final String text;
  final IconData icon;
  final Widget action;
  final Function onTab;
  final Color color;

  ListSetting({
    @required this.text,
    @required this.icon,
    @required this.action,
    this.onTab,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTab,
      child: Padding(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Card(
          color: color ?? ColorsCustom.black.shade300,
          child: SizedBox(
            height: MediaQuery.of(context).size.height / 12,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Icon(
                    icon,
                    // color: Colors.blueGrey.shade700,
                  ),
                ),
                Expanded(
                  flex: 6,
                  child: Padding(
                    padding: EdgeInsets.only(right: 10),
                    child: Padding(
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              fontWeight: FontWeight.w300,
                              // color: Colors.grey.shade800,
                              fontSize: 16,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 10),
                            child: action,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class LineSetting extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(children: <Widget>[
      Expanded(
        flex: 2,
        child: Container(),
      ),
      Expanded(
        flex: 6,
        child: Padding(
          padding: EdgeInsets.only(right: 15),
          child: Divider(
            thickness: 0.6,
          ),
        ),
      ),
    ]);
  }
}

class ForwardArrowSetting extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(),
      child: Icon(
        Icons.arrow_forward_ios,
        size: 20,
        // color: Colors.grey.shade700,
      ),
    );
  }
}

class TextHederListSetting extends StatelessWidget {
  final String text;
  TextHederListSetting(this.text);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 20, top: 10),
      child: Text(
        text,
        style: TextStyle(
          fontWeight: FontWeight.w400,
          // color: Colors.grey.shade800,
          fontSize: 18,
        ),
      ),
    );
  }
}

Widget padding(Widget widget) {
  return Padding(
    padding: EdgeInsets.only(left: 20, bottom: 10),
    child: widget,
  );
}
