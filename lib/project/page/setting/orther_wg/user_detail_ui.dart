import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:projectx/framework/service/http_client_service.dart';
import 'package:projectx/project/commons/constants/theme.dart';
import 'package:projectx/project/commons/global_var.dart';
import 'package:projectx/project/models/get_user_detail.dart';
import 'package:file_picker/file_picker.dart';
import 'package:path/path.dart' as path;

class UserDetail extends StatefulWidget {
  @override
  _UserDetailState createState() => _UserDetailState();
}

class _UserDetailState extends State<UserDetail> {
  GetUserDetail userData;
  File files;
  Future<bool> getUserDetailData() async {
    setState(() {
      userData = dataUserDetail;
    });
    return true;
  }

  void updateImageProfile() async {
    files = await FilePicker.getFile(type: FileType.image);
    FormData dataSent = FormData.fromMap({
      "image": await MultipartFile.fromFile(
        files.path,
        filename: path.basename(files.path),
      )
    });
    var response = await HttpClientService.post(
        "users/upload-img-profile", dataSent,
        isFormData: true);
    setState(() {
      userData.imgProfile = response;
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return FutureBuilder(
        future: getUserDetailData(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData)
            return Column(
              children: <Widget>[
                SizedBox(
                  height: height / 5 * 2,
                  width: width,
                  child: Stack(
                    children: <Widget>[
                      ShaderMask(
                        shaderCallback: (rect) {
                          return LinearGradient(
                            begin: Alignment.center,
                            end: Alignment.bottomCenter,
                            stops: [0.2, 1],
                            colors: [Colors.black, Colors.transparent],
                          ).createShader(
                              Rect.fromLTRB(0, 0, rect.width, rect.height));
                        },
                        blendMode: BlendMode.dstIn,
                        child: Image.network(
                          HttpClientService.contextPath +
                              'file-manager/get-img/' +
                              userData.imgProfile +
                              "?height=600&width=400",
                          height: height / 5 * 2,
                          width: width,
                          fit: BoxFit.cover,
                        ),
                      ),
                      Align(
                        alignment: Alignment.topRight,
                        child: Padding(
                          padding: EdgeInsets.only(
                              top: height / 20, right: width / 30),
                          child: FloatingActionButton(
                            backgroundColor:
                                ColorsCustom.black.shade100.withOpacity(0.6),
                            child: Icon(
                              Icons.edit,
                              color: Colors.white,
                            ),
                            onPressed: updateImageProfile,
                            mini: true,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(height / 18.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              '${userData?.firstName?.toUpperCase() ?? ""}\n${userData?.lastName?.toUpperCase() ?? ""}',
                              style: TextStyle(
                                shadows: <Shadow>[
                                  Shadow(
                                    offset: Offset(0.0, 0.0),
                                    blurRadius: 0.0,
                                    color: ColorsCustom.black,
                                  ),
                                  Shadow(
                                    offset: Offset(0.0, 0.0),
                                    blurRadius: 8,
                                    color: Colors.black,
                                  ),
                                ],
                                fontSize: 26,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              userData.jobPosition.toUpperCase(),
                              style: TextStyle(
                                fontSize: 13,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Text(
                            '฿123',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              fontFamily: fontFamilyPimary,
                            ),
                          ),
                          Text(
                            'LEAVE (Y)',
                            style: TextStyle(fontSize: 10),
                          ),
                        ],
                      ),
                      Container(
                        height: 50.0,
                        width: 1.0,
                        color: Colors.white30,
                        margin: EdgeInsets.only(left: 10.0, right: 10.0),
                      ),
                      Column(
                        children: <Widget>[
                          Text(
                            '1,200',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              fontFamily: fontFamilyPimary,
                            ),
                          ),
                          Text(
                            'FARE (M)',
                            style: TextStyle(fontSize: 10),
                          ),
                        ],
                      ),
                      Container(
                        height: 50.0,
                        width: 1.0,
                        color: Colors.white30,
                        margin: EdgeInsets.only(left: 10.0, right: 10.0),
                      ),
                      Column(
                        children: <Widget>[
                          Text(
                            '12,000',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              fontFamily: fontFamilyPimary,
                            ),
                          ),
                          Text(
                            'FARE (Y)',
                            style: TextStyle(fontSize: 10),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            );
          else
            return Center(
              child: SizedBox(
                height: (height / 5 * 2) + (height / 20),
                child: SpinKitFoldingCube(
                  color: ColorsCustom.green,
                ),
              ),
            );
        });
  }
}
