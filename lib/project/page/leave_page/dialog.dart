import 'package:flutter/material.dart';
import 'dart:math';

/* -----------------------------------  from below is test example.  -----------------------------------  */
List<Color> colorListCustom = [
  new Color(0xfff62459),
  new Color(0xff23cba7),
  new Color(0xfff7ca18),
  new Color(0xffd2527f),
  new Color(0xff9b59b6),
  new Color(0xff87d37c),
  new Color(0xff1ba39c),
  new Color(0xfff2784b),
  new Color(0xff22313f),
  new Color(0xff1e8bc3),
  new Color(0xfff03434),
  new Color(0xfff22613),
  new Color(0xffdb0a5b),
  new Color(0xff963694),
  new Color(0xff5333ed),
  new Color(0xff1e824c),
  new Color(0xfff2d984),
  new Color(0xffe67e22),
  new Color(0xfff27935),
];

class UserExample {
  String profile;
  String name;
  String email;
  Color color;
  UserExample({this.email, this.name, this.profile, this.color});
}
/* --------------------------------------------------------------------------------------------------------  */

void showCustomDialogWithImage(BuildContext context, String url) {
  Dialog dialogWithImage = Dialog(
    child: ClipRRect(
      child: Image.network(
        url,
        fit: BoxFit.cover,
      ),
    ),
  );
  showDialog(
      context: context, builder: (BuildContext context) => dialogWithImage);
}
