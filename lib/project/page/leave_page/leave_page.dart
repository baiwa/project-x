import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:projectx/project/page/chat/chat.dart';
import 'package:projectx/project/page/leave_page/dialog.dart';

import '../../commons/constants/theme.dart';

class LeavePage extends StatefulWidget {
  LeavePage({Key key}) : super(key: key);
  @override
  _LeavePageState createState() => _LeavePageState();
}

class _LeavePageState extends State<LeavePage> {
  bool heart = false;
  tapHeart() {
    if (heart) {
      setState(() {
        heart = false;
      });
    } else {
      setState(() {
        heart = true;
      });
    }
    log(heart.toString());
  }

  @override
  Widget build(BuildContext context) {
    var maxHeight = MediaQuery.of(context).size.height;
    var maxWidth = MediaQuery.of(context).size.width;
    var cardOneWidth;
    var cardTwoWidth;
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SafeArea(
          // key: _showSnackBar,
          child: ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 15, right: 15, bottom: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'News',
                  style: TextStyle(
                    fontSize: 36,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => ChatForm()),
                    );
                  },
                  child: Text(
                    "Go 2 Chat",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    SizedBox(
                      // width: maxWidth,
                      // height: maxHeight / 4,
                      child: Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment(0, 0),
                            child: SizedBox(
                                width: maxWidth / 1.2,
                                height: cardOneWidth = maxWidth / 2.5,
                                child: Padding(
                                    padding: const EdgeInsets.only(bottom: 0),
                                    child: InkWell(
                                      // onTap: showSnackBar,
                                      child: GestureDetector(
                                        onTap: () {
                                          showCustomDialogWithImage(context,
                                              'https://images.pexels.com/photos/2115217/pexels-photo-2115217.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260');
                                        },
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                          child: Image.network(
                                            'https://images.pexels.com/photos/2115217/pexels-photo-2115217.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                    ))),
                          ),
                          Align(
                              alignment: Alignment.bottomCenter,
                              child: SizedBox(
                                  // height:maxHeight,
                                  width: maxWidth / 1.25,
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                        top: cardOneWidth / 1.5),
                                    child: Card(
                                        color: Colors.grey.shade100,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                        ),
                                        child: Padding(
                                            padding: EdgeInsets.only(
                                                top: 10,
                                                right: 15,
                                                left: 20,
                                                bottom: 5),
                                            child: Column(
                                              // mainAxisSize: MainAxisSize.min,
                                              // mainAxisAlignment: MainAxisAlignment.start,
                                              children: <Widget>[
                                                ListTile(
                                                  leading: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            22.0),
                                                    child: Image.network(
                                                      'http://baiwa.co.th/uploads/staff/20190124102223tWJZU.jpg',
                                                      fit: BoxFit.cover,
                                                      height: cardOneWidth / 4,
                                                    ),
                                                  ),
                                                  title: Text(
                                                    "Andree-Ann LabranLabran LabranasdasdasdasdLabranLabranLabranLabranLabranLabranasdasdasdasdLabranLabranLabran",
                                                    maxLines: 1,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        color: ColorsCustom
                                                            .black.shade200,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  subtitle: Text(
                                                    "30 Mar",
                                                    style: TextStyle(
                                                        color: ColorsCustom
                                                            .black.shade200),
                                                  ),
                                                ),
                                                Align(
                                                  child: Text(
                                                    "We must belive that we are gifted for something, and that this thing, at whatever cost, must be attained.We must belive that we are gifted for something, and that this thing, at whatever cost, must be attained.We must belive that we are gifted for something, and that this thing, at whatever cost, must be attained.We must belive that we are gifted for something, and that this thing, at whatever cost, must be attained.We must belive that we are gifted for something, and that this thing, at whatever cost, must be attained.We must belive that we are gifted for something, and that this thing, at whatever cost, must be attained.We must belive that we are gifted for something, and that this thing, at whatever cost, must be attained.We must belive that we are gifted for something, and that this thing, at whatever cost, must be attained.We must belive that we are gifted for something, and that this thing, at whatever cost, must be attained.We must belive that we are gifted for something, and that this thing, at whatever cost, must be attained.",
                                                    // "",
                                                    style: TextStyle(
                                                        color: ColorsCustom
                                                            .black.shade100),
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    maxLines: 3,
                                                  ),
                                                  alignment: Alignment.topLeft,
                                                ),
                                                Divider(),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: <Widget>[
                                                    Center(
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        children: <Widget>[
                                                          PopupMenuButton(
                                                            icon: Icon(
                                                              Icons.share,
                                                              color:
                                                                  ColorsCustom
                                                                      .black
                                                                      .shade200,
                                                            ),
                                                            itemBuilder:
                                                                (context) => [
                                                              PopupMenuItem(
                                                                  textStyle: TextStyle(
                                                                      color: Colors.pink,
                                                                      // fontWeight:
                                                                      //     FontWeight
                                                                      //         .bold,
                                                                      fontSize: 18),
                                                                  child: Text("Love")),
                                                              PopupMenuItem(
                                                                  enabled:
                                                                      false,
                                                                  child: Icon(
                                                                    Icons.cake,
                                                                    color: Colors
                                                                        .pink,
                                                                  )),
                                                              PopupMenuItem(
                                                                  child: Icon(
                                                                Icons.camera,
                                                                color:
                                                                    Colors.pink,
                                                              )),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Row(
                                                      children: <Widget>[
                                                        Text(
                                                          "256",
                                                          style: TextStyle(
                                                              color: ColorsCustom
                                                                  .black
                                                                  .shade200),
                                                        ),
                                                        Padding(
                                                          padding: EdgeInsets.only(
                                                              right:
                                                                  cardOneWidth /
                                                                      7),
                                                          child: Icon(
                                                            Icons.comment,
                                                            color: ColorsCustom
                                                                .black.shade200,
                                                          ),
                                                        ),
                                                        GestureDetector(
                                                          onTap: () {
                                                            tapHeart();
                                                          },
                                                          child: ButtonBar(
                                                              children: heart
                                                                  ? <Widget>[
                                                                      Text(
                                                                        "4k",
                                                                        style: TextStyle(
                                                                            color:
                                                                                ColorsCustom.black.shade200),
                                                                      ),
                                                                      Icon(
                                                                        Icons
                                                                            .favorite_border,
                                                                        color: ColorsCustom
                                                                            .black
                                                                            .shade200,
                                                                      ),
                                                                    ]
                                                                  : <Widget>[
                                                                      Text(
                                                                        "4k",
                                                                        style: TextStyle(
                                                                            color:
                                                                                Colors.pink),
                                                                      ),
                                                                      Icon(
                                                                          Icons
                                                                              .favorite_border,
                                                                          color:
                                                                              Colors.pink),
                                                                    ]),
                                                        ),
                                                        // ButtonBar(

                                                        //     children:),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ))),
                                  ))),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                SizedBox(
                  // width: maxWidth,
                  height: maxHeight / 4.25,
                  child: Stack(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topCenter,
                        child: SizedBox(
                          // height: 200,

                          width: cardTwoWidth = maxWidth / 1.25,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 0),
                            child: Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              color: Colors.grey.shade100,
                              child: Padding(
                                padding: EdgeInsets.only(
                                    left: 0, bottom: 10, right: 25, top: 10),
                                child: Row(
                                  children: <Widget>[
                                    SizedBox(
                                      width: cardTwoWidth / 4,
                                    ),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        // mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            "31 Mar",
                                            style: TextStyle(
                                                color: ColorsCustom
                                                    .black.shade200),
                                          ),
                                          Text(
                                            "Adrien Stone",
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 1,
                                            style: TextStyle(
                                                color:
                                                    ColorsCustom.black.shade200,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            "if you're offered a seat on a rocket ship, don't ask what seat! Just get on. You are corona!!",
                                            // "You are corona!!",
                                            style: TextStyle(
                                                color: ColorsCustom
                                                    .black.shade100),
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 3,
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),

                      // SizedBox(height: cardTwoWidth/12),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: SizedBox(
                          width: maxWidth / 4,
                          height: maxWidth / 4,
                          child: Container(
                              // color: Colors.deepOrange,
                              child: Padding(
                            padding: const EdgeInsets.only(bottom: 0, top: 0),
                            child: GestureDetector(
                              onTap: () {
                                showCustomDialogWithImage(context,
                                    'https://image.freepik.com/free-photo/people-wearing-protective-masks-are-showing-stop-sign-by-hands_1157-31391.jpg');
                              },
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(15.0),
                                child: Image.network(
                                  'https://image.freepik.com/free-photo/people-wearing-protective-masks-are-showing-stop-sign-by-hands_1157-31391.jpg',
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          )),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                SizedBox(
                  // width: maxWidth,
                  height: maxHeight / 4.25,
                  child: Stack(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topCenter,
                        child: SizedBox(
                          // height: 200,
                          width: cardTwoWidth = maxWidth / 1.25,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 0),
                            child: Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              color: Colors.grey.shade100,
                              child: Padding(
                                padding: EdgeInsets.only(
                                    left: 25, right: 0, top: 10, bottom: 10),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        // mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            "1 Apr",
                                            style: TextStyle(
                                                color: ColorsCustom
                                                    .black.shade200),
                                          ),
                                          Text(
                                            "Bernard Nolan",
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 1,
                                            style: TextStyle(
                                                color:
                                                    ColorsCustom.black.shade200,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            "You can't fall if you don't climb. But there's no joy in living your whole life on the ground.",
                                            // "We must belive that we are gifted",
                                            style: TextStyle(
                                                color: ColorsCustom
                                                    .black.shade100),
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 3,
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      width: cardTwoWidth / 4,
                                    ),
                                    SizedBox(height: 10),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: SizedBox(
                            width: maxWidth / 4,
                            height: maxWidth / 4,
                            child: Padding(
                                padding: const EdgeInsets.only(bottom: 0),
                                child: GestureDetector(
                                  onTap: () {
                                    showCustomDialogWithImage(context,
                                        'https://image.freepik.com/free-photo/sunrise-bali-jungle_1385-1644.jpg');
                                  },
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(15.0),
                                    child: Image.network(
                                      'https://image.freepik.com/free-photo/sunrise-bali-jungle_1385-1644.jpg',
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ))),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      )),
    );
  }
}
