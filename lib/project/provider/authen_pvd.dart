import 'package:flutter/material.dart';
import 'package:projectx/framework/service/shared_preferences.dart';
import 'package:projectx/project/commons/global_var.dart';
import 'package:projectx/project/page/login_page/ui/login_main_provider.dart';

class AuthenPvd with ChangeNotifier {
  bool isLogin = false;

  init() async {
    bool hasLogin = await SharedDataLocalService.getValue<bool>("baiwa-projext-x-authen") ?? false;
    this.isLogin = hasLogin;
    String username = await SharedDataLocalService.getValue<String>("baiwa-projext-x-username") ?? null;
    userUtils = LoginResModel(username: username);
    notifyListeners();
  }

  set setIsLogin(bool login) {
    isLogin = login;
    SharedDataLocalService.writePreferences<bool>("baiwa-projext-x-authen", login);
    notifyListeners();
  }

  set setUser(String user) {
    SharedDataLocalService.writePreferences<String>("baiwa-projext-x-username", user);
  }
}
